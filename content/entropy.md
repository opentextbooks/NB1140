---
jupytext:
    formats: md:myst
    text_representation:
        extension: .md
        format_name: myst
kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---
(ch:entropy)=
# Heat engines, entropy, and free energy

## Thermodynamic processes
### Quasi-static processes

```{index} quasi-static process, thermodynamic process ; quasi-static
```
If you put an ideal gas (or any other gas for that matter) in a closed, insulated container, and leave it alone long enough, it will eventually reach thermodynamic equilibrium. If you then slowly heat or compress the gas, its temperature and/or pressure will change, putting it in a different equilibrium (there is no such thing as 'the' thermodynamic equilibrium). As long as your meddling is done slow enough, the system will essentially be in equilibrium in each of the intermediate states as well. Such a process, in which you take the system from one equilibrium to another, through a series of intermediate equilibria, is called a *quasi-static process*. Quasi-static processes are reversible: you could simply retrace your steps and end up back where you started. Naturally, there are also irreversible processes, which are essentially all processes which are not quasi-static - for example, you could compress your gas very quickly, or remove the insulation and drop your container in a cold fluid. Such a sudden, irreversible change is often referred to as a *quench* of your system.

In this section, we'll consider some common quasi-static processes, focusing on the simplest possible example, the ideal gas, to illustrate the basic ideas. As we've seen already, the state of an ideal gas is completely determined by four variables: its pressure&nbsp;$p$, temperature&nbsp;$T$, volume&nbsp;$V$, and number of molecules&nbsp;$N$ (or equivalently, the number of moles of gas&nbsp;$n$). These four are related by the ideal gas law&nbsp;{eq}`idealgaslaw`, which means that only three are independent. For our examples, we'll take a fixed amount of gas, so we are left with two independent variables, because the ideal gas law tells us that $pV/T = \mathrm{constant}$. For illustrative purposes, we'll describe our processes using a $pV$-diagram, plotting the pressure as a function of the volume. One immediate advantage is that the work done by the gas (equation&nbsp;{eq}`thermoworkonwall`) is then simply the area under the graph representing the process; of course, the work done *on* the gas (which is what we'll typically want to know) is again minus this number.

We'll calculate both the work done on the gas, and the heat flow into or out of the gas in each of the processes described below. Combined, they give the change of the internal energy of the gas, as expressed in the first law of thermodynamics (equation&nbsp;{eq}`thermofirstlaw`): $\Delta U = Q + W$. By definition, for an ideal gas there are no interactions between the particles, so there are no forces between the particles, and thus there is no potential energy associated with each particle. Consequently, the only form of energy present in an ideal gas is the kinetic energy of the particles - so the total energy can *only* depend on this kinetic energy, which is to say, on the temperature. You can find a mathematical proof of this statement in {numref}`app:idealgasinternalenergy`, where we also extend it to another, somewhat more realistic model, the van der Waals gas.

```{figure} images/thermodynamics/thermoprocesses.svg
:name: fig:thermoprocesses
$pV$&nbsp;diagrams for four quasistatic thermodynamic processes. (a) Isovolumetric process. (b) Isobaric process. (c) Isothermal process, with $pV = N\kB T = \mathrm{const}$. (d) Adiabatic process (no heat flow). Dashed lines indicate isotherms, showing that the line describing an adiabat, $pV^\gamma = \mathrm{const}$, is steeper than that of an isotherm.
```

### Isothermal processes

```{index} isothermal process, thermodynamic process ; isothermal
```
An *isothermal* process is one in which the temperature is kept constant. The associated line in the $pV$-diagram is known as an isotherm ({numref}`fig:thermoprocesses`a). For an isothermal process of an ideal gas, we can express the pressure as a function of the volume as $p(V) = n R T / V$. Because we only vary the volume $V$, all isotherms are hyperbola. Calculating the amount of work done on a gas that expands isothermally is also easy:

$$
W = - \int_{V_1}^{V_2} p \,\mathrm{d}V = - n R T \int_{V_1}^{V_2} \frac{1}{V} \mathrm{d}V = - n R T \log\left(\frac{V_2}{V_1}\right).
$$ (isothermalwork)

What about the heat flow during an isothermal process? This is the first point at which we benefit greatly from the observation in the last section that the internal energy of an ideal gas depends only on its temperature; so for an isothermal processes, in which the temperature does not change, the internal energy is constant (and hence $\Delta U = 0$). Therefore, for an isothermal process, $Q = - W = nRT \log(V_2 / V_1)$.

### Isovolumetric process

```{index} isovolumetric process, thermodynamic process ; isovolumetric
```
An  *isovolumetric* (or *isochoric*) process is one in which we keep the volume constant. The associated line in a $pV$&nbsp;diagram ({numref}`fig:thermoprocesses`b) is vertical, and we immediately see that in such a process no work is done on the gas. Because the pressure changes in an isovolumetric process, the temperature must change also, if we want to keep our process quasi-static. In an isovolumetric process the internal energy does therefore change, and because there is no work done on the gas, the change in internal energy equals the heat flow: $\Delta U = Q$. We've encountered these processes before in {numref}`sec:macroheat`, where we defined the proportionality factor relating the heat flow and the temperature at constant volume as the heat capacity&nbsp;$C_V$ (equation&nbsp;{eq}`defspecificheat`A). For an ideal gas, it is convenient to work instead with the (molar) specific heat at constant volume<sup>[^1]</sup>, defined as

$$
C_V = \frac{1}{n} \left( \frac{\mathrm{d}Q}{\mathrm{d}T} \right)_V,
$$ (defmolarspecificheat)

which gives:

$$
Q = n C_V \Delta T.
$$ (isovolumetricheat)

Equation&nbsp;{eq}`isovolumetricheat` also gives us the change in the internal energy in an isovolumetric processes. Because the internal energy of an ideal gas depends only on the temperature<sup>[^2]</sup>, the same expression holds for *any* process involving a change in temperature in an ideal gas:

$$
\Delta U = n C_V \Delta T.
$$ (internalenergyidealgas)

### Isobaric process

```{index} isobaric process, thermodynamic process ; isobaric
```
An *isobaric* process is one in which we keep the pressure constant. The associated line in a $pV$&nbsp;diagram ({numref}`fig:thermoprocesses`c) is horizontal, and again determining the amount of work done is easy:

$$
W = - p (V_2-V_1) = - p \Delta V.
$$ (isobaricwork)

Similar to an isovolumetric process, the temperature, and thus the internal energy, changes in an isobaric process. Consequently, there is again a heat flow, but this time, it is not equal to the change in internal energy, as there is also work done. However, the flow of heat is still directly proportional to the change in temperature, only the proportionality constant has changed. In other words, we have a different specific heat, the specific heat at constant pressure&nbsp;$C_p$. Where $C_V$ was the heat required to heat one mole of gas by one degree at constant volume, $C_p$ becomes the same at constant pressure. Writing down the first law of thermodynamics with all expressions we found, we have:
```{math}
:label: isobaricheat
\begin{align*}
Q &= \Delta U - W \\
n C_p \Delta T &= n C_V \Delta T + p \Delta V
\end{align*}
```
In the second line of&nbsp;{eq}`isobaricheat` we used equation&nbsp;{eq}`internalenergyidealgas` for $\Delta U$, which is after all valid in any ideal gas process. Specifically for our isobaric process, we also have a relation between $\Delta V$ and $\Delta T$, given by the ideal gas law: $p \Delta V = n R \Delta T$. Substituting this back into equation&nbsp;{eq}`isobaricheat`, we find a relation between our two specific heats:

$$
C_p = C_V + R.
$$ (idealgasspecificheats)

For an ideal gas, the amount of heat needed to raise the temperature of one mole by one degree at constant pressure therefore always exceeds that at constant volume by exactly an amount&nbsp;$R$. The difference between the two changes for different models, but it is always true that $C_p > C_V$. This makes intuitive sense: in a constant-volume process, all energy can be converted into heat, whereas in a constant-pressure process, there is also a part of the energy that gets converted into work.

### Adiabatic process

```{index} adiabatic process, thermodynamic process ; adiabatic
```
An *adiabatic* process is one in which there is no heat flow into or out of the system. The associated line in a $pV$&nbsp;diagram ({numref}`fig:thermoprocesses`d) is decreasing, and does so more rapidly than that of an isothermal process that started at the same temperature. Unlike in the previous three processes, it is not immediately clear what the function $p(V)$ describing this line should be, so we need to do some work to derive it, before we can calculate the work done in the process (which, because $\mathrm{d}Q = 0$, equals the change in the internal energy).

To find $p(V)$, we start from the first law of thermodynamics, in its differential form, and use that, for any process, $\Delta U = n C_V \Delta T$. Because $\mathrm{d}Q = 0$, we get:
```{math}
:label: adiabaticenergy
\begin{align*}
\mathrm{d}U &= \mathrm{d}Q + \mathrm{d}W = 0 - p \,\mathrm{d}V  \\
&= n C_\mathrm{V} \mathrm{d}T,
\end{align*}
```
which gives us a relation between $\mathrm{d}V$ and $\mathrm{d}T$. Differentiating the ideal gas law gives us another such relation:

$$
\mathrm{d}(pV) = p \,\mathrm{d}V + V \,\mathrm{d}p = n R \,\mathrm{d}T.
$$ (diffidealgaslaw)

Combining&nbsp;{eq}`adiabaticenergy` and&nbsp;{eq}`diffidealgaslaw`, we get

$$
V \,\mathrm{d}p = - p \,\mathrm{d}V - \frac{R}{C_V} p \,\mathrm{d}V.
$$ (adiabatdiff)

To solve equation&nbsp;{eq}`adiabatdiff`, we separate variables as usual (dividing both sides by $pV$) and integrate, to get:
```{math}
:label: adiabaticequation
\begin{align*}
\int \frac{1}{p} \mathrm{d}p &= - \int \frac{1+R/C_V}{V} \mathrm{d}V  \\
\log(p) &= - \frac{C_V+R}{C_V} \log(V) + \mathrm{const}  \\
p V^{\gamma} &= \mathrm{const},
\end{align*}
```
where

$$
\gamma = \frac{C_V+R}{C_V} = \frac{C_p}{C_V}
$$ (adiabaticparameter)

is the *adiabatic exponent*, and we've used the fact that for an ideal gas $C_p = C_V + R$.

```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter14/c14q3.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```

To find the actual value of $\gamma$, we go back to the equipartition theorem (equation&nbsp;{eq}`equipartition`), which tells us that per molecule, the internal energy of an ideal gas is $(d/2) \kB T$, where $d$ is the number of degrees of freedom. The total internal energy for $N$ molecules is then $(d/2) N \kB T = (d/2) n R T$, where $n = N/N_\mathrm{A}$ is the number of moles. Because we also have $\Delta U = n C_V T$, we can simply read off that $C_V = (d/2)R$, and $C_p = C_V + R = (1+d/2)R$, so $\gamma = \frac{1+d/2}{d/2} = 1+2/d$. For a monatomic ideal gas, $d=3$ (as the atoms can move freely in three directions), for a diatomic $d=5$ (as the molecules can also rotate around two axes, but rotating around the symmetry axis doesn't change anything), and for a triatomic non-linear gas $d=6$. For mixtures, we can have different values of $d$, and consequently of $\gamma$, but we always have $1 < \gamma < 5/3$.

It is now easy to see why the adiabatic line in a $pV$&nbsp;diagram drops faster than that of an isothermal process: for an isotherm, $p \sim 1/V$, whereas for an adiabat, $p \sim 1/V^\gamma$, with $\gamma > 1$. To determine the constant in equation&nbsp;{eq}`adiabaticequation`, we simply substitute the values of $p$ and $V$ at a known point, say $(p_1, V_1)$. Calculating the work done in an adiabatic expansion to $(p_2, V_2)$ is then easy:

$$
W = -\int_{V_1}^{V_2} \frac{p_1 V_1^\gamma}{V^\gamma} \mathrm{d}V = \frac{p_2 V_2 - p_1 V_1}{\gamma - 1}.
$$

```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter14/c14q2.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```

(sec:heatengines)=
## Cyclic processes and heat engines

```{index} cyclic process, thermodynamic process ; cyclic
```
We can combine the four types of reversible processes (or any other we like, but we'll stick to these four relatively simple ones) to create a *cyclic* reversible process: a process in which we go through a number of consecutive steps but end up at the same point where we started, the 'same point' here being the same thermodynamic state of the system, so quantities like the pressure, volume and temperature are the same at the start and the end. Consequently, any *state function*, that is to say a function that only depends on the state, such as the total internal energy, also has to be the same at the start and end of the cycle (though it can be different of course at different points in the cycle).

```{figure} images/thermodynamics/thermocycles.svg
:name: fig:thermocycles
$pV$&nbsp;diagrams for two reversible cyclic processes that can be used as heat engines. (a) Simple heat engine combining two isovolumetric and two isobaric processes. The amount of work extracted from the system equals the area enclosed by the cycle. (b)&nbsp;Carnot cycle, consisting of two isothermal and two adiabatic processes.
```

```{index} heat engine
```
The fact that we can convert thermal energy to mechanical work can be exploited by building a *heat engine*. We simply set up a cyclic process in which we extract more work from the system than we put into it, in effect converting heat to mechanical work. Using the isovolumetric and isobaric process, we can do so easily, see {numref}`fig:thermocycles`a: we first expand at constant pressure, then reduce pressure at constant volume (by cooling), compress at constant pressure, and increase pressure at constant volume (by heating). Since compressing a cooler gas requires less work, we get a net conversion of heat to work, which is the basic idea behind the steam engine and most combustion engines.

```{index} efficiency
```
Note that not all the heat that we put into the heat engine is converted to work. For a practical heat engine, we need both a hot reservoir, at temperature&nbsp;$T_\mathrm{h}$, from which we extract an amount of heat&nbsp;$Q_\mathrm{h}$, and a cold reservoir (usually the environment), at temperature&nbsp;$T_\mathrm{c}$, into which we 'dump' a (smaller) amount of heat&nbsp;$Q_\mathrm{c}$, see {numref}`fig:heatengines`a. We define the *efficiency* of the engine as the ratio between the extracted work and the input heat: $\eta \equiv W / Q_\mathrm{h}$.

```{index} reversible engine
```
If we make sure that all the processes in the cycle of our heat engine proceed in a quasi-static manner, i.e., going from one equilibrium state smoothly into one another, our engine is *reversible*: we can go through the cycle in the opposite direction, using work to transfer heat from the cold to the hot reservoir (this is how a refrigerator works, see {numref}`fig:heatengines`b). For a reversible engine, its cycle will at some point return to its original state, at which point its internal energy will be the same as at the start (as the internal energy depends only on the internal state). The first law of thermodynamics then tells us that the extracted work equals the difference between the extracted and dumped heat: $W = Q_\mathrm{h} - Q_\mathrm{c}$, and the efficiency equals

$$
\eta \equiv \frac{W}{Q_\mathrm{h}} = \frac{Q_\mathrm{h} - Q_\mathrm{c}}{Q_\mathrm{h}} = 1 - \frac{Q_\mathrm{c}}{Q_\mathrm{h}}.
$$ (thermodynamicefficiency)

```{figure} images/thermodynamics/heatengines.svg
:name: fig:heatengines
(a) A heat engine takes an amount of heat $Q_\mathrm{h}$ from a hot heat bath at temperature&nbsp;$T_\mathrm{h}$, and converts this into an amount of work&nbsp;$W$, as well as a remaining amount of heat&nbsp;$Q_\mathrm{c}$ which is dumped into a cold bath at temperature&nbsp;$T_\mathrm{c}$ (usually the environment). (b) A heat engine run in reverse is a refrigerator, extracting heat from a cold bath and dumping it in a hot bath. (c) Setup in which two reversible heat engines with different efficiencies are coupled. The work from the left engine is used to run the right engine in reverse. The result is a net heat flow from the cold to the hot heat bath, violating the second law of thermodynamics.
```

```{index} Carnot's theorem
```
You might think that the efficiency of a reversible engine depends on its design, but this turns out not to be the case; it depends only on the temperatures of the two heat baths. This result is known as **Carnot's theorem**. To see why it works, consider the opposite case. Suppose we have two reversible heat engines with different efficiencies, $\eta_1$ and $\eta_2 < \eta_1$. We then use the work extracted from the engine with the highest efficiency to run the one with the lowest efficiency in reverse, see {numref}`fig:heatengines`c. If the second engine uses all the work produced by the first, we get:

$$
\eta_2 < \eta_1 \quad \Leftrightarrow \quad \frac{W}{Q_\mathrm{h}^{(2)}} < \frac{W}{Q_\mathrm{h}^{(1)}} \quad \Rightarrow \quad Q_\mathrm{h}^{(1)} < Q_\mathrm{h}^{(2)},
$$ (thermosecondlawviolation)

so the amount of heat extracted from the hot bath by the more efficient engine is lower than the amount of heat added (to the hot bath!) by the less efficient engine. As no net work is put into the system, this heat has to come from the cold bath. This setup thus leads to a net spontaneous (because no work is added to the system) flow of heat from the cold to the hot bath, in direct violation of the second law of thermodynamics. Therefore, the efficiencies of the two reversible engines have to be identical.

```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter15/c15q2.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```
```{figure} images/quizes/c15q2_carnotengine.png
:name: fig:c15q2_carnotengine
:width: 400px
```

The fact that the efficiency of all reversible heat engines is the same means that we only need to calculate it once to get a universally valid expression for it. Moreover, we can do the calculation for a case that is designed to make that calculation easy. To that end, we'll use the Carnot cycle ({numref}`fig:thermocycles`b), which consists of two isothermal and two adiabatic processes. As we've seen in the previous section, in an isothermal process the curve in the $pV$ plane is given by $p = N \kB T / V$, and because the change in internal energy depends only on the change in temperature, for an isothermal process the internal energy is constant ($\mathrm{d}U = 0$, so $\mathrm{d}Q = - \mathrm{d}W$). For an adiabatic process, we found $p V^\gamma = \mathrm{const}$, where $\gamma = C_p/C_V$. Since $\gamma > 1$, the line describing an adiabat in the $pV$ plane is steeper than that describing an isotherm, and we can create a cycle.

Calculating the amount of work done and amounts of heat absorbed and extracted in a Carnot cycle is now a simple exercise. For the isothermal process from $(p_1, V_1)$ to $(p_2, V_2)$, we get (combining the ideal gas law, the expression for work in equation&nbsp;{eq}`thermowork`, and the isothermal property that $\mathrm{d}Q = - \mathrm{d}W$)

$$
Q_\mathrm{h} = - W = \int_{V_1}^{V_2} \frac{N \kB T_\mathrm{h}}{V} \mathrm{d}V = N \kB T_\mathrm{h} \log\left(\frac{V_2}{V_1}\right).
$$ (isothermalworkandheat)

Likewise, we find that the amount of heat dumped in the isothermal process from $(p_3, V_3)$ to $(p_4, V_4)$ is given by $Q_\mathrm{c} = N \kB T_\mathrm{c} \log(V_3 / V_4)$. Since the two adiabatic processes occur (by construction) without absorbing or dumping any heat, we can write for the efficiency:

$$
\eta_\mathrm{Carnot} = 1 - \frac{Q_\mathrm{c}}{Q_\mathrm{h}} = 1 - \frac{T_\mathrm{c}\log(V_3/V_4)}{T_\mathrm{h}\log(V_2/V_1)}.
$$ (Carnotefficiency)

Now $V_2$ and $V_3$ (and $V_4$ and $V_1$) are related by adiabatic processes, which obey $p V^\gamma = \mathrm{const}$, or, again by application of the ideal gas law, $T V^{\gamma - 1} = \mathrm{const}$, so we have $T_\mathrm{h} V_2^{\gamma-1} = T_\mathrm{c} V_3^{\gamma-1}$ and $T_\mathrm{h} V_1^{\gamma-1} = T_\mathrm{c} V_4^{\gamma-1}$. If we divide these two equations by each other, we find that $V_2 / V_1 = V_3 / V_4$, which means that the two log terms in equation&nbsp;{eq}`Carnotefficiency` are equal, and thus

$$
\eta_\mathrm{reversible} = 1 - \frac{T_\mathrm{c}}{T_\mathrm{h}}.
$$ (reversibleengineefficiency)

Irreversible heat engines will necessarily have a lower efficiency (otherwise they could be used to break Carnot's theorem). Practical heat engines, even when run at a close-to-reversible cycle, will also have a lower efficiency, as some of the work produced will necessarily dissipate due to friction.


```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter14/c14q4.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```

(sec:entropy)=
## Entropy

```{index} entropy
```
We've arrived at last at the mysterious and often ill-understood concept of entropy. Let us start with two common misconceptions. The first is that entropy is a measure of 'energy quality', that is to say, that some energy would be more useful than others. The second is that entropy is a measure of disorder - a broken egg having higher entropy than an unbroken one. Neither of these is entirely wrong, but neither is entirely correct either. Indeed, if we are given a system with a (relatively) low entropy, we will be able to construct an engine to extract work from it. And also indeed, if you compare the molecules in a drop of ink that you've just put in a glass of water (the 'more ordered' state), to the later situation in which they're dispersed throughout the water (the 'less ordered' state), the first has the lower entropy. However, counter-examples also exist. The best known one is probably the depletion interaction, in which you dissolve both relatively large colloids (spherical particles of, say, $100\;\mu\mathrm{m}$ in diameter) and smaller polymers (which form blobs of say $5\;\mu\mathrm{m}$) in water - in that case, the colloids and polymers will spontaneously demix, and the demixed state has the highest entropy. Moreover, in this example the energy is constant, so the 'quality of energy' description doesn't really apply either.

Then what is entropy? The simplest way to understand it is through a direct analogy with mechanics. In mechanics, the total energy consists of the sum of the kinetic and potential energy, and is conserved, like it is in thermodynamics. Potential energy is nothing but 'the potential to do work' (remember that the potential energy difference between A and B is simply minus the amount of work you need to do to transport something from A to B), and by the work-energy theorem, doing work results in a change of the kinetic energy. In thermodynamics, the entropy is analogous to the potential energy, with one important (historical) difference, namely its sign. While a higher potential energy means you can extract more work from the system, a lower entropy also means you can extract more work from the system. A mechanical system that is left alone will minimize its potential energy; a thermodynamic system that is left alone will maximize its entropy.

```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter15/c15q1.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```

To find a way to calculate the entropy, we return to the Carnot cycle. We found that its efficiency is given by $1-T_\mathrm{c}/T_\mathrm{h}$ (equation&nbsp;{eq}`reversibleengineefficiency`). Comparing with the &nbsp;{eq}`thermodynamicefficiency` of efficiency allows us to read off that $Q_\mathrm{c}/Q_\mathrm{h} = T_\mathrm{c}/T_\mathrm{h}$, or, after rearranging

$$
\frac{Q_\mathrm{h}}{T_\mathrm{h}} = \frac{Q_\mathrm{c}}{T_\mathrm{c}}.
$$ (reversibleengineheattemperaturerelation)

Remember that $Q_\mathrm{h}$ is the heat that flows into the engine from the hot bath at $T_\mathrm{h}$, whereas $Q_\mathrm{c}$ is the heat that flows out of the engine to the cold bath at $T_\mathrm{c}$. Equation&nbsp;{eq}`reversibleengineheattemperaturerelation` tells us that in a complete cycle, the difference between the 'heat per temperature' influx and outflux is zero. Therefore, in addition to the internal energy of the system, this 'heat per temperature' gives us a second quantity that is unchanged when we've gone through a full reversible cycle. This second quantity is the *entropy*.

You may object that, even though we've proven that the efficiency of any reversible engine must be the same, we used the specific setup of the Carnot engine to derive equation&nbsp;{eq}`reversibleengineefficiency`. For any other system (e.g., the isovolumetric/isobaric engine in {numref}`fig:thermocycles`a), heat does not flow in at a constant temperature, as the internal temperature of the engine changes under compression or expansion. You would of course be right, so we need a little care to make our expression for the entropy universally valid. The trick is to consider infinitesimal steps: let us add, reversibly, an infinitesimal amount of heat $\delta Q$ from a reservoir at temperature&nbsp;$T$. The added 'heat per temperature', or entropy, to the system is then given by

$$
\mathrm{d}S = \frac{\delta Q}{T}.
$$ (entropydifferential)

Adding heat to a system increases its entropy, removing heat (i.e., flipping the sign of $\delta Q$), removes it. For a complete Carnot cycle, we can integrate equation&nbsp;{eq}`entropydifferential` to get

$$
\Delta S_\mathrm{Carnot} = \oint \frac{\mathrm{d}Q}{T} = \frac{Q_\mathrm{h}}{T_\mathrm{h}} - \frac{Q_\mathrm{c}}{T_\mathrm{c}} = 0,
$$ (entropychangeCarnotcycle)

by equation&nbsp;{eq}`reversibleengineheattemperaturerelation`.

We can always construct a Carnot cycle between any two points $(p_1, V_1)$ and $(p_2, V_2)$ in the $p$, $V$ state space of an engine. By equation&nbsp;{eq}`entropychangeCarnotcycle`, the entropy change if we go through the entire cycle is zero, so we can define an entropy difference between the two points as

$$
\Delta S_{12} = \int_1^2 \frac{\mathrm{d}Q}{T},
$$ (defentropydifference)

where '1' and '2' refer to the points in the state space.

Equations&nbsp;{eq}`entropychangeCarnotcycle` and&nbsp;{eq}`defentropydifference` still wouldn't be much use if the change in entropy would depend on the path taken. Fortunately, that is not the case: the entropy, like the energy, is a *state function*: it depends only on the thermodynamic variables<sup>[^3]</sup>. You can easily see that this is true, as you can describe any cycle as the limit of a large number of Carnot cycles, and as equation&nbsp;{eq}`entropychangeCarnotcycle` holds for each of those Carnot cycles, it also holds for their sum, i.e., the arbitrary cycle. Therefore, the change in entropy over any closed path is zero:

$$
\Delta S_\mathrm{reversible} = \oint \frac{\mathrm{d}Q}{T} = 0,
$$ (entropychangereversiblecycle)

and the entropy difference between two points is well-defined.

As you have surely noticed, equations {eq}`entropychangereversiblecycle` and {eq}`defentropydifference` are mathematically identical (except for the minus sign) to the equations for the work done by a conservative force along a closed path and the definition of the potential energy difference between any two points (see {numref}`sec:potentialenergy`). When left to its own devices, a mechanical system, through friction and drag (non-conservative forces) will generally tend to find a local minimum of its potential energy. Likewise, thermal systems, by dissipating energy to their environment, will gradually tend to maximize their entropy. The equilibrium point (the equivalent to the minimum of the potential energy) is thus the state of maximum entropy.

```{index} Clausius theorem
```
The mathematical form of the above statement (i.e., that systems tend to maximize their entropy) is known as the *Clausius theorem*, which states that for any cycle, we have

$$
\oint \frac{\mathrm{d}Q}{T_\mathrm{env}} \leq 0,
$$ (Clausiusinequalityintegralform)

where $T_\mathrm{env}$ is the temperature of the environment (with which the system can exchange heat). When considering an infinitesimal step, we then get for the entropy of the system itself:

$$
\mathrm{d}S_\mathrm{system} \geq \frac{\delta Q}{T_\mathrm{env}}.
$$ (Clausiusinequality)

The proof of the Clausius theorem is not difficult (see {numref}`pb:Clausiustheorem`): it essentially is a direct application of the second law of thermodynamics, as heat can only flow from a reservoir into the system if the reservoir is hotter than the system, and only from the system to a reservoir if the reservoir is colder than the system. These temperature differences can be a source of heat dissipation (resulting in an increase in overall entropy) without the production of work.

A direct consequence of the Clausius theorem is that the entropy in a closed system can never decrease. Considering the universe as a whole, we can also conclude that the entropy of the entire universe can never decrease (but it can, and does, increase over time). These observations are direct consequences of the second law of thermodynamics. Alternatively, one can prove the second law by taking the entropy observations as an axiom, an approach taken in many textbooks. The major downside of that approach is that the law by itself does not tell you what entropy is, resulting in the common confusion about its meaning.

```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter13/c13q1.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```


## Thermodynamic variables

```{index} thermodynamic variables
```
Systems in the thermodynamic limit (meaning that they consist of a very large number of constituent particles) can be described by a relatively small set of variables. We've encountered several of them above: the temperature, pressure, volume, and number of particles (or number of moles) in the system. We've also seen that these variables can be related to each other through an equation of state that describes the system at hand (with the ideal gas law as our main example). Finally, we've encountered two state functions, which are functions of these variables alone: the internal energy and the entropy. Here, we'll dive into the relation between all these variables in some more detail, which will give us a universal relation between all of them.

```{index} thermodynamic variables ; intensive, thermodynamic variables ; extensive
```
When considering our thermodynamic variables, the first thing to note is that they come in two types. The first type doesn't care about the volume of the system. If the air in a room is at a certain temperature and you put a wall in the middle of the room, the temperature in the two new subrooms will still be equal. The same goes for the pressure of the air. We call these variables *intensive*. The second type scales with the size of the system - double the size, and you double the variable. Examples are the volume and the number of particles. We call these variables *extensive*.

```{index} conjugate variables
```
Unsurprisingly, the internal energy of a thermodynamic system is also an extensive variable. You can see this explicitly in the expression for the internal energy of an ideal gas (equation&nbsp;{eq}`internalenergyidealgas`), which scales with the number of moles of gas. Likewise, the heat flow $Q$ and amount of work done&nbsp;$W$ depend on the amount of gas. We already noted that the work is the product of a pair of *conjugate variables* (here the pressure and the volume); we now see that one of them is intensive, and the other extensive.

By its very definition, the entropy is also an extensive variable. As both energy and entropy are extensive, we may expect that changes in energy and entropy are related by an intensive variable, just like changes in volume and energy are related by the (intensive) pressure. To see how this works, consider a system with fixed volume&nbsp;$V$ and number of particles&nbsp;$N$. We then add an infinitesimal amount of heat&nbsp;$\delta Q$ to this system. As the volume and number of particles are held fixed, there is no net work done, so by the first law, $\mathrm{d}U = \delta Q$, and by combining this relation with equation&nbsp;{eq}`defentropydifference`, we get

$$
\left( \frac{\mathrm{d}S}{\mathrm{d}U} \right)_{V,N} = \frac{1}{T}.
$$ (entropyenergyderivative)

Perhaps unsurprisingly, the intensive variable relating energy and entropy is the temperature. Temperature and entropy are thus another pair of conjugate variables. Equation&nbsp;{eq}`entropyenergyderivative`, in combination with&nbsp;{eq}`thermowork`, allows us to re-write the first law in terms of entropy, volume, and number of particles:

$$
\boxed{\mathrm{d}U = T \,\mathrm{d}S - p \,\mathrm{d}V + \mu \,\mathrm{d}N.}
$$ (energydifferential)

```{index} chemical potential
```
Equation&nbsp;{eq}`energydifferential` is the form of the first law<sup>[^4]</sup> that is most used. We introduced the third term, $\mu \,\mathrm{d}N$, to allow for a change in the number of particles as well. $N$ is a dimensionless extensive variable, so $\mu$, known as the *chemical potential*, must be an intensive variable with the dimension of energy: it is simply the energy cost of adding one more particle at fixed volume and entropy.

Equation&nbsp;{eq}`energydifferential` tells us that we can write the internal energy $U$ as a function of the entropy, volume, and number of particles: $U = U(S, V, N)$. For each of these extensive variables, if we want to calculate the conjugate intensive variable, we take the derivative of $U$ with the other variables kept fixed, so we have:

```{math}
:label: energyderivatives
\begin{align*}
T &= \left(\frac{\mathrm{d}U}{\mathrm{d}S} \right)_{V,N}, \\
-p &= \left(\frac{\mathrm{d}U}{\mathrm{d}V} \right)_{S,N}, \\
\mu &= \left(\frac{\mathrm{d}U}{\mathrm{d}N} \right)_{S,V}.
\end{align*}
```

```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter14/c14q1.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```

(sec:freeenergyorderparameters)=
## Free energy and order parameters

(sec:freeenergy)=
### Free energy

```{index} free energy ; Helmholz, Helmholz free energy
```
We've seen in {numref}`sec:heatengines` and&nbsp;{numref}`sec:entropy` that not all internal energy of a system is available to do work. In particular for a heat engine, some of the available energy in the hot reservoir will inevitably be used to raise the temperature of the cold reservoir, and thus the total entropy of the system consisting of the engine and whatever it is working on. This principle holds generally, as a direct consequence of the second law of thermodynamics. In many practical cases, knowing the internal energy of your system is therefore not very useful. Instead, you want to know how much energy is available to do work. This quantity is known as the (Helmholz) *free energy*<sup>[^5]</sup>, which is given by<sup>[^6]</sup>

$$
F = U - TS,
$$ (HelmholzFE)

and its differential can easily be calculated to be

$$
\mathrm{d}F = \mathrm{d}U - \mathrm{d}(TS) = T \,\mathrm{d}S - p \,\mathrm{d}V + \mu \,\mathrm{d}N - T \,\mathrm{d}S - S \,\mathrm{d}T = - S \,\mathrm{d}T - p \,\mathrm{d}V + \mu \,\mathrm{d}N.
$$ (Helmholzdifferential)

```{index} free energy ; Gibbs, Gibbs free energy
```
The Helmholz free energy is a function of the system's temperature, volume, and number of particles. Like the internal energy, it is extensive, meaning that it scales with the system size. It is particularly useful for the study of solutions. There are, however, cases for which it is not the most useful choice, for example when studying gases. In that case, rather than controlling the volume, it may be easier to control the pressure of your system. Fortunately, there is also  free energy that is a function of temperature, pressure, and number of particles, known as the *Gibbs free energy*, given by
```{math}
:label: GibbsFE
\begin{align*}
G &= F + pV = E - TS + pV, \
\end{align*}
```

```{math}
:label: Gibbsdifferential
\begin{align*}
\mathrm{d}G &= - S \,\mathrm{d}T + V \,\mathrm{d}p + \mu \,\mathrm{d}N.
\end{align*}
```

```{index} free energy ; grand canonical, grand canonical free energy
```
Alternatively, you might have a system that, in addition to a reservoir of heat, has a reservoir of particles (a grand-canonical system) at a given chemical potential. In that case, the free energy would be a function of temperature, volume, and chemical potential, known as the *grand canonical free energy*
```{math}
:label: GCFE
\begin{align*}
\Phi &= F -\mu N = E - TS - \mu N, \
\end{align*}
```

```{math}
:label: GCdifferential
\begin{align*}
\mathrm{d}\Phi &= - S \,\mathrm{d}T - p \,\mathrm{d}V + N \,\mathrm{d}\mu.
\end{align*}
```

```{index} free energy ; enthalpy, enthalpy
```
Finally, in chemistry you may encounter reactions which produce or absorb heat (so the temperature is not fixed), for which the free energy is a function of entropy, pressure, and number of particles: the *enthalpy*:
```{math}
:label: enthalpy
\begin{align*}
H &= E + pV, \
\end{align*}
```

```{math}
:label: enthalpydifferential
\begin{align*}
\mathrm{d}H &= T \,\mathrm{d}S + V \,\mathrm{d}p + \mu \,\mathrm{d}N.
\end{align*}
```
All the free energies defined above are extensive. Note that you cannot define a free energy that is a function of the temperature, pressure, and chemical potential: it would be a function of intensive variables alone, and thus no longer extensive, and therefore be the same for all system sizes.


```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter15/c15q3.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```


(sec:phasetransitions)=
### Phase transitions and phase coexistence

```{index} phases
```
Large collections of particles can exhibit different levels of ordering dependent on thermodynamic parameters such as the temperature and pressure. These different levels of ordering are commonly referred to as *phases*. For a substance that consists of identical particles only (a single component system), these phases include solid, liquid and gas. We can draw a phase diagram for such a system as a function of temperature and pressure, indicating for which values of the parameters the system is in which state, as we did in {numref}`fig:phasediagram`. The boundaries between these values indicate phase transitions, at which the ordering of the system changes.

```{figure} images/thermodynamics/thermodynamicequilibrium2.svg
:name: fig:coexistingphasesequilibrium
Two coexisting phases that are separated by a boundary that allows for the transfer of heat, volume, and particles. In equilibrium, the entropy of the whole system will be maximized, resulting in a state in which the two phases have the same temperature, pressure, and chemical potential (see equation&nbsp;{eq}`deltaScoexistingphases`).
```

At a phase boundary, two phases coexist in a configuration which is in thermodynamic equilibrium. As we discussed in {numref}`sec:equilibrium`, thermodynamic equilibrium means that the system is in thermal, mechanical, and chemical equilibrium. For two coexisting phases, these conditions translate to the statements that their temperatures, pressures, and chemical potentials must be equal. We can prove this statement using the laws of thermodynamics. According to (the differential form of) the first law, equation&nbsp;{eq}`energydifferential`, any change in energy of a system in equilibrium is due to a change in entropy, volume, or number of particles. From equation&nbsp;{eq}`energydifferential`, we see that the energy itself is a function of entropy, volume, and particle number: $U = U(S, V, N)$. We can invert this function to find an expression for the entropy, which then must be a function of the energy, volume, and particle number: $S = S(U, V, N)$. The second law of thermodynamics now tells us that for any spontaneous process in a closed system, the entropy can only increase, $\Delta S \geq 0$. Consequently, the entropy in a system in equilibrium is always maximized. Now suppose we have a closed system with two coexisting phases, separated by a movable, permeating and heat-conducting wall ({numref}`fig:coexistingphasesequilibrium`). We label the energy, volume, and number of particles in the two subsystems with 1 and 2, and because the system is closed, $U = U_1 + U_2$, $V = V_1 + V_2$, and $N = N_1 + N_2$ are conserved. For any process that transfers heat, volume, or particles from one subsystem to the other, we thus have $\Delta U = \Delta U_1 = -\Delta U_2$, $\Delta V = \Delta V_1 = -\Delta V_2$, and $\Delta N = \Delta N_1 = -\Delta N_2$. For the change in entropy in the whole system as a result of this transfer, we can then write

$$
\Delta S = \left(\frac{1}{T_1} - \frac{1}{T_2}\right) \Delta U + \left(\frac{p_1}{T_1} - \frac{p_2}{T_2}\right) \Delta V + \left(\frac{\mu_1}{T_1} - \frac{\mu_2}{T_2}\right) \Delta N.
$$ (deltaScoexistingphases)

Now because $S$ attains a maximum at equilibrium, we must have $\Delta S = 0$, which can only be satisfied if $T_1 = T_2$, $p_1 = p_2$, and $\mu_1 = \mu_2$.

(sec:orderparameters)=
### Order parameters and phase transitions

```{index} order parameter
```
A phase transition is usually related to the breaking of a symmetry of the system. Simple examples include the liquid-to-solid phase transition, in which translational symmetry is broken (liquids are homogeneous, while crystals only have a discrete symmetry), the demixing of two fluids (from a single to two coexisting homogeneous states) and the paramagnetic (isotropic) to ferromagnetic (ordered) transition, which leads to the creation of a magnet when cooling down certain metals or alloys (known as ferromagnets<sup>[^7]</sup>). The breaking of such a symmetry is usually described by a change in a thermodynamic function known as the *order parameter*, with the defining property that they are different in each phase. Order parameters are usually (though not always) chosen such that they are zero in the more symmetric phase, and nonzero in the broken-symmetry phase. Unfortunately, there is no general procedure for selecting a 'good' (as in, easy to work with while physically accurate) order parameter; finding one that is appropriate to the system at hand is therefore a bit of an art. In some cases, the selection is easy though. For the onset of magnetization, we choose the net magnetization $\bm{m}$, defined as the magnetic dipole moment per unit volume of the material. For the gas-to-liquid transition we use the density&nbsp;$\rho$ (or the difference between the density and the density at coexistence, $\rho - \rho_\mathrm{c}$), and for the demixing transition the volume fraction&nbsp;$\phi$ of one of the components as our order parameter.

```{index} Landau theory
```
As systems tend to spontaneously minimize their free energy (which free energy, like the order parameter, depends on the system), we can find the state of a system by minimizing the free energy as a function of the order parameter. Although we usually have no explicit expression for this function, close to a phase transition we can expand it in a Taylor series around the value at coexistence. This approach was pioneered by the Russian physicist Lev Landau, and is therefore known as *Landau theory* of phase transitions. We'll illustrate the idea here with the simplest possible case, in which the free energy&nbsp;$F$ is a symmetric function of an order parameter&nbsp;$\phi$ close to the coexistence line. As constant terms are irrelevant in any energy, the first two nontrivial terms are then the square and fourth order terms in&nbsp;$\phi$, and we can write

$$
F(\phi, T) = F_2(T) \phi^2 + F_4(T) \phi^4.
$$ (Landaufreeenergyexpansion)

The function $F_4(T)$ has to be strictly positive, as otherwise the energy would strictly decrease for large values of&nbsp;$\phi$, and thus the system would automatically evolve to the limit that $\phi$ diverges. The function $F_2(T)$ can be negative or positive. In the latter case, we have a single minimum, but in the former case, we get two, with a local maximum in between at $\phi = 0$; that is the case for which we find phase coexistence. In {numref}`fig:Landauphasediagram`, some examples are plotted for free energies at various positions in the phase diagram close to the coexistence line of the liquid and gas phase.

```{figure} images/thermodynamics/phasediagramwithenergyplots.svg
:name: fig:Landauphasediagram
Phase diagram with sketches of the free energy close to and on the coexistence line, at the critical point, and 'beyond' the critical point (on the extrapolated coexistence line).
```

If you move along the phase coexistence line in {numref}`fig:Landauphasediagram` (i.e., move from (c) to (e) to (f)), the local maximum gets less high, until it disappears at (f). There, the term $F_2(T)$ in the Landau expansion&nbsp;{eq}`Landaufreeenergyexpansion` equals zero, and the corresponding $F(\phi)$ curve is very flat in the center, as not only its first, but also its second and third derivatives vanish. Beyond this point, $F_2(T)$ becomes positive, and only a single minimum remains ({numref}`fig:Landauphasediagram`g). Point (f) is thus the critical point, where the difference between the liquid and gas phase disappears.

## Relations between the intensive variables

(sec:GibbsDuhem)=
### The Gibbs-Duhem relation

Although we've split the concept of 'thermodynamic equilibrium' into three constitutive parts, in practice you can't separate them fully. That is not due to lack of experimental skill, but due to a fundamental constraint in thermodynamics: a relation between the intensive variables of a system that fixes one of them as a function of the others. For example, in a single-component system, once you've set the temperature and pressure, the chemical potential is also set.

To derive the relation between the intensive variables, we'll start from the Gibbs free energy, $G = G(T, p, N)$, which is a function of two intensive variables, temperature and pressure, and a single extensive variable, the number of particles. Because any (free) energy is also extensive, we know that $G$ must scale linearly with $N$. We can therefore define a Gibbs free energy per particle, which is a function of $T$ and $p$ alone:

$$
g(T, p) = \frac{G(T, p, N)}{N}.
$$ (GibbsFEperparticle)

We can substitute $G = N g$ in the expression for the differential $\mathrm{d}G$ of $G$, equation&nbsp;{eq}`Gibbsdifferential`, to get an expression for the differential of $g$:

$$
\mathrm{d}G = \mathrm{d}(Ng) = N \,\mathrm{d}g + g \,\mathrm{d}N.
$$ (Gibbsperparticledifferential)

Comparing equations&nbsp;{eq}`Gibbsdifferential` and&nbsp;{eq}`Gibbsperparticledifferential`, we can read off that
```{math}
:label: Gibbsperparticledifferential2
\begin{align*}
N \,\mathrm{d}g &= - S \,\mathrm{d}T + V \,\mathrm{d}p, \
\end{align*}
```

```{math}
:label: Gibbsperparticlechempot
\begin{align*}
g &= \mu.
\end{align*}
```

```{index} Gibbs-Duhem relation
```
The Gibbs free energy per particle thus equals the chemical potential<sup>[^8]</sup> (the energy you need to add a single particle). Substituting that equation into equation&nbsp;{eq}`Gibbsperparticledifferential2` gives us the *Gibbs-Duhem relation* between the intensive variables:

$$
\boxed{N \,\mathrm{d}\mu = - S \,\mathrm{d}T + V \,\mathrm{d}p.}
$$ (GibbsDuhem)

The Gibbs-Duhem relation can be extended to systems containing multiple components (the term on the left-hand side is then replaced by $\sum_i N_i \,\mathrm{d}\mu_i$, with the sum running over all components). We can also formalize it one step further, and replace all extensive variables except the entropy with an abstract extensive variable $X_i$ (sometimes called a 'flux'), paired with an intensive variable&nbsp;$f_i$ (called a 'force'). In that case, the (internal) energy is a function of $S$ and the $X_j$'s, the temperature is still $(\partial U/\partial S)_{X_i}$, and the forces are the other derivatives, $f_i = (\partial U/\partial X_i)_{T,X_{j \neq i}}$. The Gibbs-Duhem relation then reads:

$$
S \,\mathrm{d}T + \sum_i X_i \,\mathrm{d}f_i = 0.
$$ (GibbsDuhemgeneral)

(sec:Gibbsphaserule)=
### The Gibbs phase rule

When you mix multiple components, additional phases appear, that are enriched in some of the components. Such phases may be solid, liquid or gaseous in nature, and like in the one-component case, multiple phases can coexist in equilibrium. Coexistence lines may also still end in critical points as a function of temperature, pressure, or another thermodynamic variable. Unlike the one-component case, more than two phases can coexist for a range of values of these variables. The maximum number of coexisting phases that can exist in a system is given by the *Gibbs phase rule*:

$$
\#\text{Phases} = C - F + 2,
$$ (Gibbsphaserule)

where $C$ is the number of components, and $F$ the number of degrees of freedom, i.e., the number of intensive variables (such as temperature and pressure) that can be varied simultaneously and arbitrarily without determining one another. For a one-component system, we can get at most three phases if we fix both temperature and pressure (i.e., put $F=0$, at the unique triple point), while for a three-component system we could thus get up to five coexisting phases. The Gibbs phase rule follows from the fact that the intensive thermodynamic properties are not independent, as they are related by the Gibbs-Duhem relation&nbsp;{eq}`GibbsDuhem`. When the pressure and temperature are variable, the composition of a phase with $C$ components is thus set by specifying $C-1$ chemical potentials, as the last one follows from the Gibbs-Duhem relation. If there are $P$ phases coexisting at a point, there are a total of $(C+1)P$ variables to be set (the $C-1$ chemical potentials, the temperature, and the pressure in each phase) with $C(P-1)+2(P-1)$ constraints, as the chemical potentials of all components, temperatures, and pressures must be equal across the phases. The number of degrees of freedom is given by the number of variables minus the number of constraints, which gives $F = (C+1)P - C(P-1) - 2(P-1) = C - P + 2$, or equation&nbsp;{eq}`Gibbsphaserule`.

(app:idealgasinternalenergy)=
## Appendix: The internal energy of an ideal gas

In this appendix we will show that the internal energy of an ideal gas is independent of its pressure and volume, and therefore a function of its temperature and number of particles only. This feature is a direct consequence of the equation of state of the ideal gas, also known as the ideal gas law (equation&nbsp;{eq}`idealgaslaw`): $pV = N \kB T$. In our derivation, we make use of the differential form of the first law of thermodynamics (equation&nbsp;{eq}`energydifferential`), taking the number of particles to be constant, and writing $\mathrm{d}U = T \,\mathrm{d}S - p \,\mathrm{d}V$. With the number of particles constant, the system in general is a function of two independent parameters, which we are free to choose. We choose the temperature (obviously) and the volume. Then $U = U(T,V)$ and $S = S(T,V)$, so

$$
\mathrm{d}U = T \,\mathrm{d}S - p \,\mathrm{d}V = T \left. \frac{\partial S}{\partial T} \right|_V \mathrm{d}T + T \left. \frac{\partial S}{\partial V} \right|_T \mathrm{d}V - p \,\mathrm{d}V,
$$ (energydifferentialexpansion)

and

$$
\left. \frac{\partial U}{\partial V} \right|_V = T \left. \frac{\partial S}{\partial V} \right|_T- p.
$$ (energyderivative)

Since we have no expression for $S$, we need to get an alternative for the derivative on the right of equation&nbsp;{eq}`energyderivative`. We will do so using the Helmholz free energy, $F = U - TS$, and the general mathematical properties of a total differential. Suppose we have a general function of two variables $G(x,y)$, and its differential is given by $\mathrm{d}G = X \mathrm{d}x + Y \mathrm{d}y$, then we have:

$$
\left. \frac{\partial X}{\partial y} \right|_x = \left. \frac{\partial Y}{\partial x} \right|_y = \frac{\partial^2 G}{\partial x \partial y}.
$$ (generalmaxwellrelation)

Applying this to the differentials of the various free energies and other thermodynamic functions leads to a range of very useful thermodynamic relations, known as the *Maxwell relations*. Here we will apply equation&nbsp;{eq}`generalmaxwellrelation` to the differential of the Helmholz free energy, $\mathrm{d}F = - S \,\mathrm{d}T - p \,\mathrm{d}V$, which gives

$$
\left. \frac{\partial S}{\partial V} \right|_T = \left. \frac{\partial p}{\partial T} \right|_V.
$$ (maxwellSp)

Substituting&nbsp;{eq}`maxwellSp` in&nbsp;{eq}`energyderivative` and using the ideal gas law, we find

$$
\left. \frac{\partial E}{\partial V} \right|_V = T \left. \frac{\partial p}{\partial T} \right|_V- p = T \frac{p}{T} - p = 0,
$$ (energyderivative2)

so the energy is indeed independent of the volume, and a function of the temperature alone. By integrating equation&nbsp;{eq}`energyderivative2`, we find that this is true for any equation of state of the form

$$
p = f(V) T,
$$ (generalizedidealgas)

where $f(V)$ is any function of $V$, as long as it is independent of $T$. Equation&nbsp;{eq}`energyderivative2` still holds and the internal energy is a function of the temperature alone. Specifically, this result also holds for the van der Waals gas, which is an equation of state for a fluid consisting of particles that are not finite and do exert pairwise attractive forces (such as the van der Waals force) on each other. The van der Waals equation of state is given by

$$
\left[p + a \left(\frac{N}{V}\right)^2\right]( V - b N) = N \kB T,
$$ (vdWeos)

where $a$ is a measure of the attraction between the particles, and $b$ the volume excluded by a particle.

```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter13/c13q2.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```


## Problems
```{exercise} A simple heat engine
:label: pb:squarecycleengine
:class: dropdown
Possibly the simplest heat engine is the 'square cycle' in {numref}`fig:thermocycles`a.
1. Find the efficiency of this heat engine.
1. The efficiency you found at (a) is lower than that of a Carnot cycle operating between the same two temperature extremes. Why does this engine not break Carnot's theorem?
```

```{exercise} Stirling engine
:label: pb:Stirlingengine
:class: dropdown
Building a Carnot engine, although theoretically possible, is practically challenging. A much simpler design that yet works well is the *Stirling engine*, see figure. The corresponding cycle consists of four steps: (1) Isothermal expansion, (2) Isovolumetric cooling, (3) Isothermal compression and (4) Isovolumetric heating.
1. Draw the Stirling cycle in a ($p, V$) diagram.
1. Calculate the values of the pressure and volume at each of the vertices of the cycle if the cold reservoir is at $T_\mathrm{c}$, the hot reservoir at $T_\mathrm{h}$, and the initial pressure and volume are $p_1$ and $V_1$, respectively.
1. Calculate the amount of work that can be extracted in a Stirling cycle.
1. Show that the theoretical efficiency of the Stirling cycle is identical to that of the Carnot cycle (as it should be based on Carnot's theorem).
```

````{exercise} The Clausius theorem for cyclic processes
:label: pb:Clausiustheorem
:class: dropdown
The *Clausius theorem* states that in any cyclic process (both reversible and irreversible), we have (equation&nbsp;{eq}`Clausiusinequalityintegralform`):
```{math}
\oint \frac{\mathrm{d}Q}{T_\mathrm{env}} \leq 0,
```
where $T_\mathrm{surr}$ is the temperature of the environment. Equivalently, for each infinitesimal step, the entropy change of the system itself needs to be larger or equal to that of the environment (equation&nbsp;{eq}`Clausiusinequality`):
```{math}
\mathrm{d}S_\mathrm{system} \geq \frac{\delta Q}{T_\mathrm{env}}.
```
1. To prove the theorem, we first consider a step in which the system absorbs heat from the environment without any work being done. Argue that for that to be possible, the temperature of the environment, $T_\mathrm{surr}$, must be higher than that of the system at this point, $T_1$, and from that show that minus the decrease in the entropy of the environment, $-\mathrm{d}S_\mathrm{surr}$ must be less than or equal to the increase in entropy of the system, $\mathrm{d}S_\mathrm{sys}$.
1. Likewise, consider a step in which the system (now at a higher temperature&nbsp;$T_2$) expels heat to the environment, and show that for such a step, the increase in the entropy of the environment must be greater than or equal to minus the decrease in the entropy of the system.
1. From your results at (a) and (b), show that
    ```{math}
    - \oint \mathrm{d}S_\mathrm{surr} \leq \oint \mathrm{d}S_\mathrm{sys}.
    ```
1. By relating the change in entropy of the environment to the heat flow, show that if the system itself goes through a reversible cyclic process, the heat flow from the environment to the system must satisfy the Clausius theorem, equation&nbsp;{eq}`Clausiusinequalityintegralform`.
1. Finally, invert the statement in (d) to show that any system left alone and free to exchange heat with its environment will always tend to maximize its entropy.
````

[^1]: It would perhaps be more consistent to use a small $c$ for the molar specific heat, just like we did for the mass specific heat in {numref}`sec:heatcapacity`. However, most texts reserve the small letter for the mass specific heat and use the capital for the molar specific heat, a convention we'll follow here.

[^2]: As follows from the equipartition theorem, {numref}`sec:equipartitiontheorem`, but can also be proven directly from the ideal gas law, see {numref}`app:idealgasinternalenergy`.

[^3]: Thermodynamic variables describe the state of the system - e.g., the volume, pressure, temperature, and number of particles. As these are also related to each other by one or more equations of state, the energy and entropy are in practice given as functions of fewer variables. For instance, for an ideal gas, the state is fixed when setting the temperature, volume, and number of particles, as we can then calculate the pressure using the ideal gas law.

[^4]: Sometimes called the 'combined first and second law'.

```{index} thermodynamic potentials, Legendre transforms
```
[^5]: Free energies are sometimes called *thermodynamic potentials*, and transformations of the type given by equations&nbsp;{eq}`HelmholzFE`, {eq}`GibbsFE`, {eq}`GCFE`, and {eq}`enthalpy` are frequently referred to as *Legendre transforms*.

[^6]: Sadly the symbols used for the various free energies are not universally agreed upon. In particular, you will frequently find $A$ for the Helmholz free energy. To be sure, always check of which variables the given free energy is a function.

[^7]: Most notably iron (hence the name), but also cobalt and nickel, as well as a range of 'rare-earth' metals, various alloys and metal oxides, especially iron(III)oxide (Fe$_2$O$_3$).

[^8]: Note that both $g$ and $\mu$ in equation&nbsp;{eq}`Gibbsperparticlechempot` are functions of $T$ and $p$.

