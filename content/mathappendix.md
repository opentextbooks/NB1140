(app:math)=
# Math

```{figure} images/appendices/xkcd_advanced_techniques.png
Warning: Knowing math may give you mythical powers. Cartoon by Randall Munroe, [xkcd.com/2595](https://xkcd.com/2595/), CC-BY-NC 2.5.
```