(app:eqsconstants)=
# Some equations and constants

## Physical constants
```{table} Physical constants
:name: table:physicalconstants
| **Name** | **Symbol** | **Value** |
| :--- | :--: | :--- |
| Speed of light | $c$ | $3.00 \cdot 10^{8}\;\mathrm{m}/\mathrm{s}$ |
| Elementary charge | $e$ | $1.60 \cdot 10^{-19}\;\mathrm{C}$ |
| Electron mass | $m_e$ | $9.11 \cdot 10^{-31}\;\mathrm{kg} = 0.511\;\mathrm{MeV}/c^2$ |
| Proton mass | $m_p$ | $1.67 \cdot 10^{-27}\;\mathrm{kg} = 938\;\mathrm{MeV}/c^2$ |
| Gravitational constant | $G$ | $6.67 \cdot 10^{-11}\;\mathrm{N}\cdot\mathrm{m}^2/\mathrm{kg}^2$|
| Gravitational acceleration | $g$ | $9.81\;\mathrm{m}/\mathrm{s}^2$|
| Boltzmann's constant | $\kB$ | $1.38 \cdot 10^{-23}\;\mathrm{J}/\mathrm{K}$|
| Gas constant | $R$ | $8.31 \; \mathrm{J}/\mathrm{K} \cdot \mathrm{mol}$|
| Stefan-Boltzmann constant | $\sigma$ | $5.67 \cdot 10^{-8} \; \mathrm{W}/\mathrm{m}^2 \cdot \mathrm{K}^4$|
| Avogadro's number | $N_\mathrm{A}$ | $6.02 \cdot 10^{23} \; \mathrm{mol}^{-1}$|
| Planck's constant | $h$ | $6.63 \cdot 10^{-34}\;\mathrm{J}\cdot\mathrm{s}$|
| | $\hbar = h / 2 \pi$ | $1.05 \cdot 10^{-34}\;\mathrm{J}\cdot\mathrm{s}$|
```

## Moments of inertia
```{table} Moments of inertia, all about axes of symmetry through the center of mass.
:name: table:momentsofinertia
| **Object** | **Moment of inertia**  |
| :--- | :--- |
| Thin stick (length $L$) | $\frac{1}{12} M L^2$ |
| Ring or hollow cylinder (radius $R$) | $M R^2$ |
| Disk or solid cylinder (radius $R$) | $\frac12 M R^2$ |
| Hollow sphere (radius $R$) | $\frac23 M R^2$ |
| Solid sphere (radius $R$) | $\frac25 M R^2$ |
| Rectangle (size $a \times b$), perpendicular axis | $\frac{1}{12} M (a^2 + b^2)$ |
| Rectangle (size $a \times b$), axis parallel to side $b$ | $\frac{1}{12} M a^2$ |
```

## Solar system objects
```{table} Characteristics of the Sun, Earth and Moon.
:name: tab:SunEarthMoon
| | Sun | Earth | Moon |
| :--- | :--: | :--: | :--: |
| Mass (kg) | $1.99 \cdot 10^{30}$ | $5.97 \cdot 10^{24}$ | $7.35 \cdot 10^{22}$ |
| Mean radius (m) | $6.96 \cdot 10^8$ | $6.37 \cdot 10^6$ | $1.74 \cdot 10^6$ |
| Orbital period (s) | $6 \cdot 10^{15}$ | $3.16 \cdot 10^7$ | $2.36 \cdot 10^6$ |
| | (200 My) | (365.25 days) | (27.3 days) |
| Mean orbital radius (m) | $2.6 \cdot 10^{20}$ | $1.50 \cdot 10^{11}$ | $3.85 \cdot 10^{8}$ |
| Mean density (kg/m$^3$) | $1.4 \cdot 10^3$ | $5.5 \cdot 10^3$ | $3.3 \cdot 10^3$|
```

```{table} Properties of a number of solar system objects.
:name: tab:solarsystem
| Name | Symbol | Equatorial radius | Mass | Mean orbit radius | Orbital period | Inclination | Orbital eccentricity | Rotation period | Confirmed moons | Axial tilt|
| :--- | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: |
| Mercury | &#x263F; | 0.382 | 0.06 | 0.39 | 0.24 | 3.38 | 0.206 | 58.64 | 0 | 0.04|
| Venus | &#x2640; | 0.949 | 0.82 | 0.72 | 0.62 | 3.86 | 0.007 | -243.02 | 0 | 177.36|
| Earth | &#x2641; | 1 | 1 | 1 | 1 | 7.25 | 0.017 | 1 | 1 | 23.44|
| Moon | &#x263E; | 0.272 | 0.0123 | 384399 | 27.32158 | 18.29-28.58 | 0.0549 | 27.32158 | 0 | 6.68|
| Mars | &#x2642; | 0.532 | 0.107 | 1.52 | 1.88 | 5.65 | 0.093 | 1.03 | 2 | 25.19|
| Ceres | &#x26B3; | 0.0742 | 0.00016 | 2.766 | 4.599 | 10.59 | 0.08 | 0.3781 | 0 | 4|
| Jupiter | &#x2643; | 11.209 | 317.8 | 5.2 | 11.86 | 6.09 | 0.048 | 0.41 | 69 | 3.13|
| Io | | 0.285 | 0.015 | 421600 | 1.769 | 0.04 | 0.0041 | 1.769 | 0 | 0|
| Europa | | 0.246 | 0.008 | 670900 | 3.551 | 0.47 | 0.009 | 3.551 | 0 | 0|
| Ganymede | | 0.413 | 0.025 | 1070400 | 7.155 | 1.85 | 0.0013 | 7.155 | 0 | 0|
| Callisto | | 0.378 | 0.018 | 1882700 | 16.689 | 0.2 | 0.0074 | 16.689 | 0 | 0|
| Saturn | &#x2644; | 9.449 | 95.2 | 9.54 | 29.46 | 5.51 | 0.054 | 0.43 | 62 | 26.73|
| Titan | | 0.404 | 0.023 | 1221870 | 15.945 | 0.33 | 0.0288 | 15.945 | 0 | 0|
| Uranus | &#x26E2; | 4.007 | 14.6 | 19.22 | 84.01 | 6.48 | 0.047 | -0.72 | 27 | 97.77|
| Oberon | | 0.119 | 0.00051 | 583519 | 13.46 | 0.1 | 0.0014 | 13.46 | 0 | 0|
| Neptune | &#x2646; | 3.883 | 17.2 | 30.06 | 164.8 | 6.43 | 0.009 | 0.67 | 14 | 28.32|
| Triton | | 0.212 | 0.00358 | 354759 | 5.877 | 157 | 0.00002 | 5.877 | 0 | 0|
| Pluto | &#x2647; | 0.186 | 0.0022 | 39.482 | 247.9 | 17.14 | 0.25 | 6.39 | 5 | 119.59|
| Charon | | 0.095 | 0.00025 | 17536 | 6.387 | 0.001 | 0.0022 | 6.387 | 0 | unknown|
| Haumea | | 0.13 | 0.0007 | 43.335 | 285.4 | 28.19 | 0.19 | 0.167 | 2 | unknown|
| Makemake | | 0.11 | unknown | 45.792 | 309.9 | 28.96 | 0.16 | unknown | 1 | unknown|
| Eris | | 0.18 | 0.0028 | 67.668 | 557 | 44.19 | 0.44 | unknown | 1 | unknown|
```
Properties of a number of solar system objects. Equatorial radii and masses are compared to those of Earth (see {numref}`tab:SunEarthMoon`). Orbital properties are around primary (the sun for (dwarf) planets, the planet for moons). Orbital radii and periods for planets again compared to Earth, for moons in kilograms and days. Rotation period for all objects in days. Inclination and axial tilt in degrees. Data from NASA planetary fact sheets <sup>[^1]</sup>.

(app:materialproperties)=
## Material properties

```{table} Properties of some common solid materials.
:name: tab:solidmaterialproperties
| material | $\rho$ ($\mathrm{kg}/\mathrm{m}^3$) | $E$ ($\mathrm{GPa}$) | $B$ ($\mathrm{GPa}$) | $G$ ($\mathrm{GPa}$) | $c$ ($\mathrm{J}/\mathrm{kg}\cdot\mathrm{K}$) | $k$ ($\mathrm{W}/\mathrm{m}\cdot\mathrm{K}$) |
| :--- | :--: | :--: | :--: | :--: | :--: | :--: |
| aluminium | 2700 | 70 | 75 | 25 | 900 | 237 |
| bone | | 16 | 8 | 80 | | |
| concrete | 2400 | 20 | | | | |
| copper | 8940 | 110 | 140 | 44 | 386 | 401 |
| glass | | 60 | 50 | 25 | 753 | 0.8 |
| granite | | 45 | 45 | 20 | | |
| hardwood | 700 | 15 |  | 10 | | |
| iron | 7870 | 210 | 160 | 77 | 447 | 80.4 |
| marble | | 60 | 70 | 20 | | |
| steel | | 200 | 160 | 75 | | |
| ice ($-10^\circ\mathrm{C}$) | 916.7 | | | | 2050 | |
| pine wood | 373 | 7.5 |  | 5 | 1400 | 0.11 |
```
Properties of some common solid materials, measured at room temperature and pressure, unless indicated otherwise: densities&nbsp;$\rho$, Young's&nbsp;$E$, bulk&nbsp;$B$ and shear $G$ moduli, specific heats per unit mass&nbsp;$c$ and thermal conductivities per unit length&nbsp;$k$. The '$\mathrm{G}$' in $\mathrm{GPa}$ stands for 'giga', or $10^9$.

```{table} Properties of some common liquid materials
:name: tab:liquidmaterialproperties
| material | $\rho$ ($\mathrm{kg}/\mathrm{m}^3$) | $\eta$ ($\mathrm{Pa}\cdot\mathrm{s}$) | $T_\mathrm{m}$ ($\mathrm{K}$) | $L_\mathrm{f}$ ($\mathrm{kJ}/\mathrm{kg}$) | $T_\mathrm{b}$ ($\mathrm{K}$) | $L_\mathrm{v}$ ($\mathrm{kJ}/\mathrm{kg}$) | $c$ ($\mathrm{J}/\mathrm{kg}\cdot\mathrm{K}$) |
| :--- | :--: | :--: | :--: | :--: | :--: | :--: | :--: |
| ethanol | 789 | $1.07 \cdot 10^{-3}$ | 159 | 109 | 351 | 879 | 2.43 |
| glycerol | 1260 | 1.2 | 291 | 201 | 563 | 1007 | 2437 |
| mercury | 13530 | $1.53 \cdot 10^{-3}$ | 234 | 11.3 | 630 | 296 | 140 |
| water | 1000 |  $8.94 \cdot 10^{-4}$ | 273 | 334 | 373 | 2257 | 4184 |
```
Properties of some common liquid materials: densities&nbsp;$\rho$, viscosities&nbsp;$\eta$, melting and boiling points $T_\mathrm{m}$ and $T_\mathrm{b}$, heats of fusion&nbsp;$L_\mathrm{f}$ and vaporization&nbsp;$L_\mathrm{v}$ and specific heats per unit mass&nbsp;$c$. Densities, viscosities and specific heats measured at room temperature and pressure.

```{table} Melting and boiling points of some common materials, with their respective heats of transformation, measured at atmospheric pressure.
:name: tab:meltingandboiling
| Material | melting point ($K$) | $L_\mathrm{f}\;(\mathrm{kJ}/\mathrm{kg})$ | boiling point ($K$) | $L_\mathrm{v}\;(\mathrm{kJ}/\mathrm{kg})$ |
| :--- | :--: | :--: | :--: | :--: |
| alcohol | 159 | 109 | 351 | 879 |
| copper | 1357 | 205 | 2840 | 4726 |
| lead | 601 | 24.7 | 2013 | 858 |
| mercury | 234 | 11.3 | 630 | 296 |
| water | 273 | 334 | 373 | 2257 |
```

## Equations
(sec:vectorderivativeexpressions)=
### Vector derivatives

Gradient:

$$
\bm{\nabla} f(\bm{r}) = \bm{\nabla} f(x, y, z) = \left( \begin{array}{c} \partial_x f \\ \partial_y f \\ \partial_z f \end{array}\right) = \left( \frac{\partial f}{\partial x} \hat{x} + \frac{\partial f}{\partial y} \hat{y} + \frac{\partial f}{\partial z} \hat{z} \right).
$$ (defgradientCartesian)

Divergence:

$$
\bm{\nabla} \cdot \bm{v} = \left( \partial_x, \partial_y, \partial_z \right) \cdot \left( \begin{array}{c} v_x \\ v_y \\ v_z \end{array} \right) = \frac{\partial v_x}{\partial x} + \frac{\partial v_y}{\partial y} + \frac{\partial v_z}{\partial z}.
$$ (defdivergenceCartesian)

Curl:

$$
\bm{\nabla} \times \bm{A} = \left( \partial_x, \partial_y, \partial_z \right) \times \left( \begin{array}{c} A_x \\ A_y \\ A_z \end{array} \right) = \left( \begin{array}{c} \partial_y A_z - \partial_z A_y \\ \partial_z A_x - \partial_x A_z \\ \partial_x A_y - \partial_y A_x \end{array} \right).
$$ (defcurlCartesian)

[^1]: Data from NASA planetary fact sheets at [https://nssdc.gsfc.nasa.gov/planetary/planetfact.html](https://nssdc.gsfc.nasa.gov/planetary/planetfact.html).

