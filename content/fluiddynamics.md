---
jupytext:
    formats: md:myst
    text_representation:
        extension: .md
        format_name: myst
kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---
(ch:fluiddynamics)=
# Fluid dynamics

## Fluid statics: some classical results
(sec:hydropressure)=
### Hydrostatic pressure

The air pressure at sea level is always roughly 1 atmosphere, or about $10^5\;\mathrm{Pa}$, due to the combined weight of the air column directly above any given point. Going to higher ground, for example up a mountain, means that there is less air above you pressing down, and consequently the air pressure goes down when you go up, though you have to climb pretty high to start noticing. In contrast, when you dive, the pressure on your body goes up rapidly with depth, with about an atmosphere per 10 meters(!). The reason for the difference of course is that water is much heavier than air, so it weighs down on you with much larger force. To calculate that force, consider the situation sketched in {numref}`fig:hydrostaticpressure`. The air pressure at sea level ($z=z_0 = 0$) is $p = p_0$, and we take $z$ to go up in the downwards direction (so it measures depth). Consider a volume of water that extends from $z_1$ to $z_2$. The force of gravity on this volume is $F_z = mg = \rho V g$, where $\rho$ is the water's (mass) density and $V$ the volume. Our chosen volume is just water, exactly like its surroundings, and in equilibrium, so the net force on it must be zero. Therefore, the net force upward from all the water below (a normal force on the bottom surface of our body), given by $F_2 = p_2 A$, must equal the sum of the gravitational force on our volume and the downward force of everything above, $F_1 = p_1 A$ (equal but opposite to the normal force our volume exerts on everything above). The force balance thus gives (dividing by $A$ everywhere):

$$
p_2 = p_1 + \rho \Delta z g,
$$ (hydropressure1)

where $\Delta z$ is the height of our volume, and of course $V = \Delta z A$. An increase in depth of $\Delta z$ thus causes an increase in pressure $\Delta p = \rho g \Delta z$. Setting $z=0$ at the surface of the water, we can immediately calculate the pressure at depth $z$:

$$
p = p_0 + \rho g z.
$$ (hydropressure)

The pressure given in equation&nbsp;{eq}`hydropressure` is known as the *hydrostatic pressure*, and the term $\rho g z$ is the *gauge pressure* at depth $z$.

```{figure} images/continuumdynamics/hydrostaticpressure.svg
:name: fig:hydrostaticpressure
:width: 300
Calculation of the hydrostatic pressure at depth $z$.
```

### Pascal's principle

```{index} Pascal's principle
```
Unlike gases, which you can easily compress, all liquids are nearly *incompressible*<sup>[^1]</sup>. Being incompressible has an immediate and very useful consequence, known as **Pascal's principle**: the pressure in an enclosed incompressible fluid must be the same everywhere. Or, to put it differently, a change in pressure applied to an enclosed incompressible fluid is transmitted instantaneously and undiminished to every part of the fluid, and to the walls of its container. In everyday life, you encounter this principle when squeezing a tube of ketchup or toothpaste to force the fluid out; in engineering, this principle is used in the design of the hydraulic lever. An hydraulic lever, illustrated in {numref}`fig:hydrauliclever`, is a tool to lift heavy objects, such as cars, using only a small force. By applying a downward force&nbsp;$F_1$ to the small piston of area&nbsp;$A_1$ on the input side, the pressure in the fluid is increased by $p = F_1/A_1$, creating an upward force $F_2 = p A_2 = F_1 (A_2/A_1)$ on the output side. The force is thus multiplied by the ratio of the areas of the two pistons.

```{figure} images/continuumdynamics/hydrauliclever.svg
:name: fig:hydrauliclever
:width: 300
Illustration of the hydraulic lever, which exploits Pascal's principle to raise heavy objects with small forces.
```


```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter11/c11q5.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```


### Archimedes' principle and buoyancy force

```{index} buoyancy force, Archimedes' principle
```
If you drop a stone in water, it will sink, because the stone's density is higher than that of water. The force due to the upward pressure exerted by the water on the stone is smaller than the force due to gravity on the stone, hence the sinking. Conversely, an empty plastic bottle will float on water, as its density is lower than that of water, and the net upward force of the water exceeds the downward force of gravity. This upward force is called the *buoyancy force*. Naturally the buoyancy force is counteracted by the gravitational force on the object. This observation gives us a way of determining the magnitude of the force. For water in equilibrium, as in {numref}`fig:hydrostaticpressure`, the net forces up and down must balance, so the buoyancy force on the volume of water equals its weight: $F_\mathrm{b} = m_\mathrm{water} g = \rho V g$. If we replace the water by another object, the force of gravity changes, but the buoyancy force does not. We thus arrive at **Archimedes' principle**: the buoyancy force on an object equals the gravitational force on an equal size volume of water, and is directed upward: 

$$
\bm{F}_\mathrm{b} = \rho_\mathrm{water} V g \hat{\bm{z}}.
$$ (buoyancyforce)

Archimedes' principle also applies to partially submerged (floating) objects, if we only count the volume of the submerged part. For floating, we also always have a balance between the buoyancy and gravitational force. We say that a floating body displaces its own weight of water. Loading cargo on a ship therefore puts it deeper in the water: as the mass of the ship increases, it sinks a little, submerging a larger volume, which results in a larger buoyancy force, until equilibrium is re-established.

For a floating body the buoyancy force balances the gravitational force, but the floating object may still be unstable. This instability arises because the buoyancy force only counts the submerged part of the object. It therefore does not act at the center of mass of the object, as the gravitational force does, but at the center of buoyancy, which is what would be the center of mass of the water that is now displaced by the object. This center of buoyancy is often below the center of mass, which means that for any alignment away from perfect vertical, the gravitational and buoyancy forces will generate torques on the object, causing it to capsize, as often happens with toy boats in a tub. To prevent this from happening, one has to lower the boat's center of mass, which is commonly done by attaching heavy weights to the bottom of the boat.

```{index} apparent weight
```
Finally, thanks to the buoyancy force, lifting something in water is a lot easier than it is on land, because the buoyancy force helps. We can therefore assign an *apparent weight* to objects in water, equal to their actual weight minus the buoyancy force.



```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter12/c12q1.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```
```{figure} images/quizes/c12q1_meltingice.png
:name: fig:c12q1_meltingice
:width: 300px
```

(sec:surfacetension)=
### Surface tension, wetting angle, and capillary forces

```{index} surface tension
```
Famously, [Gerridae insects](https://en.wikipedia.org/wiki/Gerridae) (water striders) are able to walk on water - walk, not float - due to the *surface tension* present at the air-water interface. The surface tension originates from the fact that in a liquid the molecules are closely packed, and thus exert relatively strong attractive van der Waals forces on each other, whereas in the air, the density of molecules is much lower. Therefore molecules on the water surface experience a net force towards the bulk of the water.

The surface tension of a liquid is defined as the force per unit length along any line on the liquid's surface. It is therefore measured in Newtons per meter. Surface tension is the force responsible for creating spherical drops (drops of nearly all known liquids are spherical in the absence of gravity, as can be seen in videos from the International Space Station). The reason is simple: multiply surface tension by the surface area and you get the energy associated with the surface. As always, nature tends to minimize the energy, and the sphere is the shape that minimizes the total area given the enclosed volume.

Naturally, another way to lower the surface energy (the energy associated with the surface tension) is to simply reduce the radius of the drop, as this reduces its surface area. To do so you need to compress the liquid inside, raising its pressure. There is an energy associated with that as well: pressure times volume also gives an energy. The total energy of a drop of radius&nbsp;$R$ is then given by:

$$
E = E_\mathrm{surface} + E_\mathrm{volume} = 4\pi R^2 \sigma + \frac43 \pi R^3 \Delta p.
$$ (dropenergy)

Here $\Delta p$ is the difference in pressure between the inside and the outside of the drop. Further compressing the drop will further increase&nbsp;$\Delta p$, raising the energy. The energy of equation&nbsp;{eq}`dropenergy` thus has one term which tends to reduce the radius, and one which counteracts that, so there must be a value of&nbsp;$R$ for which they balance. To find that value, we simply take the derivative with respect to&nbsp;$R$ and set that equal to zero, which gives:

$$
\Delta p = \frac{2\sigma}{R}.
$$ (LaplacePressure)

```{index} Laplace pressure
```
The pressure difference $\Delta p$ given in equation&nbsp;{eq}`LaplacePressure` is known as the **Laplace pressure**.

```{index} wetting contact angle
```
The liquid-gas interface is not the only surface at which surface tensions exist: they are present as well at the liquid-solid and solid-gas interface, as shown in {numref}`fig:wettingangle`. If you put a drop of water on a table, the water-air surface will make a nonzero *contact angle* with the table surface. The contact angle is due to the three tensions balancing each other. In {numref}`fig:wettingangle`, the solid-gas tension balances the solid-liquid tension plus part of the liquid-gas tension, which allows us to calculate the contact angle:

$$
\cos\theta = \frac{\sigma_\mathrm{sg} - \sigma_\mathrm{sl}}{\sigma_\mathrm{lg}}.
$$ (contactangle)

```{index} complete wetting
```
Note that when we decrease $\sigma_\mathrm{lg}$ or $\sigma_\mathrm{sl}$, or increase $\sigma_\mathrm{sg}$, $\cos\theta$ approaches $1$, and the contact angle becomes smaller, until it vanishes. At that point the liquid no longer forms drops on the solid surface, but spreads out as a thin film covering the whole surface, a phenomenon known as *complete wetting*. Complete wetting occurs when $\cos\theta \geq 1$, or

$$
\sigma_\mathrm{lg} \leq \sigma_\mathrm{sg} - \sigma_\mathrm{sl}.
$$ (completewetting)

```{figure} images/continuumdynamics/wettingangle.svg
:name: fig:wettingangle
:width: 400
Surface tensions on three interfaces, balancing at a contact point.
```

```{index} capillary force
```
The interplay between the three different surface tensions of the three different phases is responsible not only for droplet formation, but also for generating *capillary forces*, which cause liquids to spontaneously rise in narrow tubes or capillaries, see {numref}`fig:capillaryforce`. Inside the capillary of radius&nbsp;$r$, the balance of surface tensions forces the water-air surface to meet the solid capillary wall at an angle&nbsp;$\theta$, pulling the water surface up with a force equal to $2\pi r \sigma \cos\theta$. The force of gravity pulls the water inside the capillary down with a force $\pi r^2 h \rho g$, where $\rho$ is the water's density. The resulting height of the water column inside the capillary is found by equating these two forces, which gives:

$$
h = \frac{2 \sigma \cos\theta}{\rho g r}.
$$ (capillaryheight)

Note that $h \sim 1/r$: the capillary effect is stronger for narrower tubes.

```{figure} images/continuumdynamics/capillaryforce.svg
:name: fig:capillaryforce
:width: 500
Capillary forces.
```

(sec:fluidflowdescriptions)=
## Descriptions of fluid flows

(sec:streamlines)=
### Eulerian picture: streamlines

When you look at water flowing out of a tub or pouring down a waterfall, you observe the fluid flow by from a fixed position as an outside observer. At any given moment, you can take a picture of the whole (spatially extended) flow. If you take two pictures in close succession, you can determine the velocity at every point in the flow (by looking at displacements of individual particles in the flow), and thus determine the instantaneous velocity field.

```{index} fluid dynamics ; Eulerian reference frame, Eulerian reference frame, fluid dynamics ; streamlines, streamlines
```
We call the 'outside observer picture' the *Eulerian reference frame*. An often-used visualization of the flow that corresponds to this reference frame is a collection of *streamlines*: tangent lines to the fluid flow. In steady (time-independent) flow, the streamlines are constant over time, and coincide with the actual paths the fluid particles take. The volume around a streamline bounded by nearby streamlines is called a flow tube ({numref}`fig:streampathlines`(a)). When the streamlines get closer, the flow tube narrows; as we'll see in {numref}`sec:continuityeq`, the continuity equation then implies that the flow speed gets higher. Streamlines in a fluid flow thus closely resemble electric or magnetic field lines, where a higher density of lines corresponds to a stronger field. 

```{figure} images/continuumdynamics/streamlines2.svg
:name: fig:streampathlines
:width: 500
Visualization of fluid flow. (a) Streamlines and a flow tube. (b) A flow field in two dimensions, with streamlines (in red) and pathlines (in green).
```

If you know the function for the streamline, finding the velocity field is simply a matter of calculating the tangent vector to it. If the streamline is parametrized by a variable $s$ and given by $\bm{x}(s) = (x(s), y(s), z(s))$, we have:

$$
\frac{\mathrm{d}\bm{x}}{\mathrm{d}s} = \bm{v}(\bm{x}(s), t),
$$ (streamlinesparametrized)

where $\bm{v} = (v_x, v_y, v_z)$ is the flow velocity. Inversely, if you know the velocity field, the streamlines can be found from a set of differential equations<sup>[^2]</sup>:

$$
\frac{\mathrm{d}x}{v_x} = \frac{\mathrm{d}y}{v_y} = \frac{\mathrm{d}z}{v_z}.
$$ (streamlines)

As an example, we consider the two-dimensional flow field $\bm{v} = (v_0/L)(x, -y)$. Equation&nbsp;{eq}`streamlines` then gives $\mathrm{d}x / x = -\mathrm{d}y / y$, which we can integrate to find $y(x) = A/x$, where $A$ is an integration constant. We find a different streamline for every choice of $A$. The vector field corresponding to the flow field and a couple of streamlines are sketched in {numref}`fig:streampathlines`(b).


```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter12/c12q2.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```
```{figure} images/quizes/c12q2_streamlinesandpathlines.png
:name: fig:c12q2_streamlinesandpathlines
:width: 300px
```


(sec:pathlines)=
### Lagrangian picture: pathlines

```{index} fluid dynamics ; Lagrangian reference frame, Lagrangian reference frame, fluid dynamics ; pathlines, pathlines
```
Instead of looking at the instantaneous flow from the viewpoint of an outside observer, you can also get in a boat and flow along with the river, or, more accurately, follow the path of a single 'fluid particle' through space and time. This co-moving point of view is known as the *Lagrangian reference frame*. Each individual particle also traces out a line as it moves through space, known as the *pathline* of the particle. If the position of the particle as a function of time is $\bm{x}_\mathrm{P}(t)$, its pathline is given by the equation:

$$
\frac{\mathrm{d}\bm{x}_\mathrm{P}}{\mathrm{d}t} = \bm{v}(\bm{x}_\mathrm{P}(t), t),
$$ (pathline)

where $\bm{v}(\bm{x},t)$ is again the fluid flow field. For a steady (time-independent) flow, pathlines and streamlines coincide, but for a time-dependent flow they are, in general, different.

Continuing our example from {numref}`sec:streamlines`, we can construct the pathlines for the two-dimensional flow field $\bm{v} = (v_0/L)(x, -y)$. Equation&nbsp;{eq}`pathline` then gives two differential equations for the $x$ and $y$ components of the particle's position. In this example, the differential equations are uncoupled, so they can be integrated independently, to give $x_\mathrm{P}(t) = x_0 \exp(v_0t/L)$ and $y_\mathrm{P}(t) = y_0 \exp(-v_0t/L)$, where $(x_0, y_0)$ is the position of the particle at $t=0$. A couple of pathlines are sketched in green in {numref}`fig:streampathlines`(b). As in this example the flow field is independent of time, the streamlines and pathlines coincide, as we can easily check by multiplying the two components: $x_\mathrm{P}(t) y_\mathrm{P}(t) = x_0 y_0 = \mathrm{const}$, so they satisfy the same equation as the streamlines.

### Streaklines

```{index} streaklines
```
A third method of visualizing a flow is by means of *streaklines*, the collection of particles in a flow that have all passed through a certain point in space. Streaklines are the lines you get when you continually inject a dye (e.g. ink) into the flow at a fixed point. Streaklines are a kind of intermediate between the Eulerian and Lagrangian reference frame, and as such less useful for analysis, though of course easier to realize in an experiment. For steady flows they coincide with the pathlines and streamlines, but for time-dependent flows they differ from both.

## Continuity
### Volume flow rate
{numref}`fig:continuitypipe` illustrates the flow of an incompressible fluid through a round pipe with varying radius. Because the fluid is incompressible, the amount of fluid that enters the pipe on the left each second must equal the amount of fluid that exits it on the right each second. Suppose that in a time $\mathrm{d}t$ a volume $V = A_1 \mathrm{d}x_1 = A_1 v_1 \mathrm{d}t$ enters the pipe on the left; then an equal volume $V = A_2 \mathrm{d}x_2 = A_2 v_2 \mathrm{d}t$ must exit on the right, and we find that:

$$
A_1 v_1 = A_2 v_2 = R_\mathrm{V}.
$$ (volumeflowrate)

```{index} fluid dynamics ; volume flow rate
```
The quantity&nbsp;$R_\mathrm{V}$ defined in equation&nbsp;{eq}`volumeflowrate` is known as the *volume flow rate*, given by the cross-sectional area of the pipe times the fluid speed, so it has the dimensions of a volume per unit time. Because for an incompressible fluid the volume flow rate is conserved, we find that for the flow of such a fluid through a pipe, the speed of the fluid is inversely proportional to the cross-sectional area of the pipe. To experience this effect for yourself, simply squeeze a tube (say a garden hose) through which water flows, and you'll find that the water will exit much faster.

In addition to the volume flow rate, we can also define the mass flow rate $R_\mathrm{m}$ by multiplying $R_\mathrm{V}$ by $\rho$: $R_\mathrm{m} = \rho A v$, which of course is conserved as well.

```{figure} images/continuumdynamics/continuity.svg
:name: fig:continuitypipe
:width: 300
If an incompressible fluid flows through this narrowing pipe, the volume flow per second cannot change. Therefore, in the narrower section of the pipe the fluid's speed must be larger.
```

```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter12/c12q4.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```
```{figure} images/quizes/c12q4_narrowingpipe.png
:name: fig:c12q4_narrowingpipe
:width: 300px
```

(sec:continuityeq)=
### The equation of continuity*

In the previous section, we looked at a very specific configuration, the flow of an incompressible fluid through a cylindrical pipe. Similar arguments apply to flows in general, and we can use them to derive a very useful property of fluid flows: the equation of continuity. Consider a small volume&nbsp;$V$ somewhere inside the fluid (see {numref}`fig:continuitybox`; this is often called a *fluid element*), and recall that the mass of this volume is given by the integral over its density:

$$
m = \int \rho \,\mathrm{d}V,
$$ (fluidmass)

which simplifies to $m = \rho V$ in the case of constant density&nbsp;$\rho$. We now consider the *flux*&nbsp;$\phi$ of fluid into the volume&nbsp;$V$. For any area bounding the volume, the flux through that area is defined as

$$
\phi = \rho \bm{v} \cdot \mathrm{d}\bm{A},
$$ (defmassflux)

where $\mathrm{d}\bm{A}$ is a vector whose length scales with the size of the area, and whose direction is perpendicular to the surface. Equation&nbsp;{eq}`defmassflux` simplifies to $\phi = \rho v \mathrm{d}A$ if $\bm{v}$ is perpendicular to the area&nbsp;$\mathrm{d}A$, so the flux is simply the mass flow rate of the previous section, but now generalized to arbitrary volumes with arbitrary boundaries. In the situation illustrated in {numref}`fig:continuitybox`, fluid is flowing in on the left (which gives us a negative flux if we define $\mathrm{d}\bm{A}$ to face outward) and out on the right (positive flux), and the net flux out is the difference between the two. Again, we can easily generalize to arbitrary volumes/surfaces by summing all contributions over all pieces of surface area $\mathrm{d}\bm{A}$, which gives us a surface integral:

$$
\phi_\mathrm{net} = \oint \rho \bm{v} \cdot \mathrm{d}\bm{A}.
$$ (netflux)

A positive net flux corresponds to a net decrease in mass of the volume element per unit time, so

$$
\phi_\mathrm{net} = - \frac{\partial m}{\partial t} = - \frac{\partial}{\partial t} \int \rho \,\mathrm{d}V = -\int \frac{\partial \rho}{\partial t} \mathrm{d}V,
$$ (netfluxmass)

where the last equality holds because we consider a fixed volume&nbsp;$V$. We now find that the volume integral on the right hand side of equation&nbsp;{eq}`netfluxmass` equals the surface integral in equation&nbsp;{eq}`netflux`. We use Stokes' theorem (sometimes known as Green's theorem) to convert that surface integral into a volume integral, and after rearranging terms we find:

$$
0 = \int \left[ \frac{\partial \rho}{\partial t} + \nabla \cdot (\rho \bm{v}) \right] \mathrm{d}V.
$$ (continuityintegral)

```{index} fluid dynamics ; continuity equation, continuity equation
```
Equation&nbsp;{eq}`continuityintegral` holds for any volume&nbsp;$V$, which can only be true if the integrand equals zero. We therefore arrive at the *continuity equation*:

$$
\frac{\partial \rho}{\partial t} + \nabla \cdot (\rho \bm{v}) = 0.
$$ (continuitygeneral)

Equation&nbsp;{eq}`continuitygeneral` allows for changes in the density of the fluid in both time and space. In many cases however the density is constant (so the fluid is incompressible), and equation&nbsp;{eq}`continuitygeneral` simplifies to

$$
\nabla \cdot \bm{v} = 0.
$$ (continuity)

Because, in contrast to gases, liquids are essentially incompressible, for liquids we'll use equation&nbsp;{eq}`continuity`. Because it is used so often, is also commonly referred to as the continuity equation.

```{figure} images/continuumdynamics/continuitybox.svg
:name: fig:continuitybox
:width: 300
Flux $\phi$ into (red) and out of (blue) a small volume of fluid.
```

(sec:streamfunction)=
### Stream function

For incompressible flows, the equation of continuity&nbsp;{eq}`continuity` gives us a relation between the components of the fluid velocity function&nbsp;$\bm{v}$. In two dimensions, this relation allows us to describe all properties of the flow using a scalar function. The equation $\partial_x v_x + \partial_y v_y = 0$ is satisfied by taking $v_x = \partial_y \psi(x,y)$ and $v_y = -\partial_x \psi(x,y)$ for any function $\psi(x, y)$. The function $\psi$ is known as the *stream function*, because the lines along which $\psi$ is constant are exactly the streamlines of {numref}`sec:streamlines`.

(sec:idealfluids)=
## Ideal fluid flow

```{index} ideal fluid
```
Our ultimate goal when studying fluid dynamics problems is to find the fluid flow field&nbsp;$\bm{v}(\bm{x}, t)$. The equation of continuity gives us a useful relation between the components of the flow field, but it does not contain enough information to give us the complete function. Fortunately, as we are dealing with classical fluids, all the laws of classical mechanics  apply. In particular, we can invoke both Newton's laws of motion and the various conservation laws. In this section, we will first apply conservation of energy to a constant flow, to derive an equation for that flow, known as Bernoulli's equation. We will then proceed to study a subclass of flows that can be described as the gradient of a scalar function, known as potential flows. Finally, we describe time-dependent flows by applying Newton's second law of motion to fluid flows. Throughout this section, we will consider *ideal* fluids only, which means that there is no friction, and hence no energy dissipation: the viscosity of the fluid is zero. We will re-introduce viscosity and dissipation in {numref}`sec:NSeq`, and look at dissipation-dominated flows in {numref}`sec:Stokeseq`. For this section, however, the fluids are ideal, so there is no dissipation, and mechanical energy is conserved.

(sec:Bernoulli)=
### Bernoulli's equation

We are now ready to apply our first result from mechanics to fluid dynamics: we'll use conservation of energy to derive Bernoulli's equation for incompressible, steady flow. Consider an arbitrary flow tube (or flow through an actual tube), such as sketched in {numref}`fig:bernoulliflowtube`. Fluid enters the tube with speed&nbsp;$v_1$ and at pressure&nbsp;$p_1$; the cross-sectional area of the tube at this point is&nbsp;$A_1$. A small fluid element of volume $V$ then takes up a length $\mathrm{d}x = V / A_1$ of the tube. We wish to know with which speed&nbsp;$v_2$ the fluid leaves the flow tube at point 2. If $v_2 \neq v_1$, then the kinetic energy of the fluid element changes by an amount&nbsp;$\Delta K = \frac12 \rho V (v_2^2 - v_1^2)$. Because energy is conserved, this means that the fluid has done work (if the kinetic energy has decreased) or something has done work on the fluid (if it has increased). For the situation as sketched in {numref}`fig:bernoulliflowtube`, there are two forces that can do work: a difference in the pressure, and gravity. If there is a height difference&nbsp;$h$ between points 2 and 1, gravity does an amount of work equal to $W_z = -\rho V g h$ on our fluid element. Note the sign: gravity points downwards, so an increase in height means that work has been done against the direction of gravity. Second, if the pressure&nbsp;$p_2$ at point 2 differs from that at point 1, the amount of work done by the pressure gradient equals $W_p = -(p_2 - p_1) V$. Equating the amount of work done to the change in kinetic energy, rearranging, and dividing by the volume (which occurs in each term, as it should, because our result should be independent of the chosen volume), we find:

$$
p_1 + \frac12 \rho v_1^2 + \rho g z_1 = p_2 + \frac12 \rho v_2^2 + \rho g z_2,
$$ (Bernoulli1)

where $h = z_2 - z_1$, and $z_i$ is the $z$-coordinate of point $i$. Because there is nothing special about points 1 and 2, equation&nbsp;{eq}`Bernoulli1` must hold for any two points on our flow tube, and we have

$$
p + \frac12 \rho v^2 + \rho g h = \text{constant}.
$$ (Bernoulli2)

```{index} Bernoulli's equation
```
Equations&nbsp;{eq}`Bernoulli1` and&nbsp;{eq}`Bernoulli2` are two versions of **Bernoulli's equation**.

```{figure} images/continuumdynamics/streamlinesbernoulli.svg
:name: fig:bernoulliflowtube
:width: 400
Flow tube for Bernoulli's equation. The green fluid elements are identical, which means that they contain the same amount of fluid, and, for an incompressible fluid, the same volume. Conservation of energy will give us a relation between the relevant physical quantities at the two different points of the flow shown here.
```

To get some insight into what Bernoulli's equation means, let us consider some special cases. First, suppose that $v_1 = v_2 =0$. Then Bernoulli's equation reads $p_2 = p_1 + \rho g (z_1-z_2)$, which is simply equation&nbsp;{eq}`hydropressure` for the hydrostatic pressure that we found in {numref}`sec:hydropressure`. Another special case is $z_1 = z_2$, or $h=0$, for which we get:

$$
p_1 + \frac12 \rho v_1^2 = p_2 + \frac12 \rho v_2^2.
$$ (Bernoulli3)

We therefore see that when streamlines are close together, we get not only high velocity, but also low pressure, otherwise equation&nbsp;{eq}`Bernoulli3` could not be satisfied. Finally, if $p_2 = p_1$, we find that $\frac12 \rho v_1^2 + \rho g z_1 = \frac12 \rho v_2^2 + \rho g z_2$, which is as much as saying that water always finds its way to the lowest point.

```{code-cell} ipython3
:tags: ["remove-input"]

import json
from jupyterquiz import display_quiz

with open("./quizes/chapter12/c12q3.json", "r", encoding="utf-8") as f:
    questions = json.load(f)

display_quiz(questions, shuffle_answers=False)
```
```{figure} images/quizes/c12q3_pingpongball.png
:name: fig:c12q3_pingpongball
:width: 300px
```


(sec:potentialflow)=
### Potential flows

An important subclass of ideal fluid flows are the cases for which the flow field can be written as the gradient of a scalar function, known as the flow potential $\phi(\bm{x})$. For a potential flow we thus have

$$
\bm{v} = \bm{\nabla} \phi.
$$ (potentialflow)

For (nearly) incompressible fluids, most flow fields away from rigid boundaries are well approximated by potential flows. At the boundaries, viscous terms are often important, but at larger distances the flow usually returns to the potential one.

If the fluid is fully incompressible, the continuity equation&nbsp;{eq}`continuity` gives $\bm{\nabla} \cdot \bm{v} = 0$, so for a potential flow we find

$$
\nabla^2 \phi = 0,
$$ (potentialflowLaplace)

where $\nabla^2 = \bm{\nabla} \cdot \bm{\nabla}$ is known as the *Laplacian*. Equation&nbsp;{eq}`potentialflowLaplace` is consequently known as *Laplace's equation*. Laplace's equation occurs throughout physics in many seemingly unrelated phenomena, including elasticity, electrostatics, heat flow, and gravitation. If the right-hand side of equation&nbsp;{eq}`potentialflowLaplace` is a nonzero constant, the equation is known as Poisson's equation. Solutions to Laplace's equation are known as *harmonic functions*.

As Laplace's equation gives us a second-order differential equation for the flow potential in space, we need boundary conditions to find specific solutions. These boundary conditions can be set at infinity (at which we usually prescribe a zero or constant flow) and at any surface. For an ideal fluid at a surface, there are no restrictions on the component of the flow along the surface, as an ideal flow has no viscosity and therefore feels no friction (this obviously is the main difference between ideal and real flows; for a real flow, we usually impose a *no-slip* boundary condition, which states that the fluid cannot move with respect to the wall). However, an ideal fluid cannot cross a rigid boundary, nor move away from it, so we have a boundary condition on the component of the flow perpendicular to the wall. Let $\bm{\hat{n}}$ be the normal vector of the wall, then the 'no flow' condition simply reads:

$$
0 = \bm{\hat{n}} \cdot \bm{v} = \bm{\hat{n}} \cdot \bm{\nabla} \phi \equiv \frac{\partial \phi}{\partial n},
$$ (potentialflowbc)

where the last equality in equation&nbsp;{eq}`potentialflowbc` defines the *normal derivative* of $\phi$: the component of the gradient along the (normal) direction&nbsp;$\bm{\hat{n}}$.

#### Worked example: potential flow along a cylinder
Find the potential flow past a rigid cylinder of radius&nbsp;$a$, placed at the origin with the long axis along the $z$-direction, for the case that the flow at infinity is given by $\bm{v} = v_0 \bm{\hat{x}}$.

#### Solution
This problem is most easily solved in cylindrical coordinates, or, as the $z$-direction is irrelevant, in polar coordinates. We can find or look up the solutions to Laplace's equation in polar coordinates, which are given by

$$
\phi(r, \theta) = \log(r), \quad \phi(r, \theta) = r^n \cos\theta, \quad \text{and} \quad \phi(r, \theta) = r^n \sin\theta,
$$

for any nonzero integer value of $n$. The general solution is a linear combination of all these solutions. For our boundary conditions, we have

$$
\left. \frac{\partial \phi}{\partial r} \right|_{r=a} = 0, \quad \text{and} \quad \lim_{r \to \infty} \bm{\nabla} \phi \to v_0 \bm{\hat{x}}.
$$

In the general solution, the derivatives of the terms with negative $n$ all decay to zero as $n \to \infty$, but the derivatives of the terms with $n > 1$ all blow up (and thus violate our boundary condition). The term with $n=1$ and the cosine gives us the desired flow as $r \to \infty$: $v_0 \bm{\nabla} (r \cos \theta) = v_0 \bm{\nabla} x = v_0 \bm{\hat{x}}$. At the surface of the cylinder, the derivative of this term gives us $v_0 \cos\theta$, which we have to compensate with one (or more, if one would not be sufficient) of the decaying terms; here we find that the cosine term with $n=-1$ will do the job, and we find for the flow potential and associated flow field:
```{math}
\begin{align*}
\phi(r, \theta) &= v_0 \left( r + \frac{a^2}{r}\right) \cos\theta = v_0 x \left(1 + \frac{a^2}{x^2 + y^2}\right), \\
\bm{v}(x, y) &= v_0 \left(1 - \frac{a^2 (x^2 - y^2)}{(x^2 + y^2)^2} \right) \bm{\hat{x}} - v_0 \frac{2 a^2 x y}{(x^2 + y^2)^2} \bm{\hat{y}}.
\end{align*}
```
The corresponding flow is visualized with streamlines in {numref}`fig:potentialflowpastcylinder`.

```{figure} images/continuumdynamics/potentialflowpastcylinder.svg
:name: fig:potentialflowpastcylinder
Streamlines of the ideal potential flow past a rigid sphere.
```

### Material derivatives*
Newton's second law of motion ({numref}`sec:Newtonslaws`) gives us a relation between the force exerted on some object and the resulting acceleration of that object. The acceleration is the time derivative of the velocity, so we need to find that time derivative. We need to be careful though: the function&nbsp;$\bm{v}(\bm{x}, t)$ does not describe the velocity of a single particle, but that of the fluid at position&nbsp;$\bm{x}$ and time&nbsp;$t$. For a particle, the position changes over time, so the position&nbsp;$\bm{x}$ becomes a function of time itself. To find the associated acceleration, we need to take the total derivative of the function $\bm{v}$ with respect to time, using the chain rule (here applied to three dimensions):
```{math}
:label: materialderivative
\begin{align*}
\bm{a} &= \frac{\mathrm{d}\bm{v}}{\mathrm{d}t} \\
&= \frac{\partial \bm{v}}{\partial t} + \frac{\partial \bm{v}}{\partial x} \frac{\partial x}{\partial t} +\frac{\partial \bm{v}}{\partial y} \frac{\partial y}{\partial t} + \frac{\partial \bm{v}}{\partial z} \frac{\partial z}{\partial t} \\
&= \frac{\partial \bm{v}}{\partial t} + v_x \frac{\partial \bm{v}}{\partial x} + v_y \frac{\partial \bm{v}}{\partial y} + v_z \frac{\partial \bm{v}}{\partial z} \\
&= \frac{\partial \bm{v}}{\partial t} + (\bm{v} \cdot \bm{\nabla}) \bm{v} \\
&\equiv \frac{\mathrm{D} \bm{v}}{\mathrm{D}t}.
\end{align*}
```

```{index} material derivative
```
The quantity&nbsp;$\mathrm{D}\bm{v}/\mathrm{D}t$ defined in equation&nbsp;{eq}`materialderivative` is known as the *material derivative* of the vector function&nbsp;$\bm{v}$. It consists of two parts: the *local derivative* $\partial_t \bm{v}$ and the *convective derivative* $(\bm{v} \cdot \bm{\nabla}) \bm{v}$. The convective term may seem a little confusing at first, but it is simply shorthand notation for the written-out expression in the third line of equation&nbsp;{eq}`materialderivative`. The material derivative is an example of a *total derivative* (hence the straight $\mathrm{d}$s in the first line of equation&nbsp;{eq}`materialderivative`), which occurs so often that it got its own notation with capital $\mathrm{D}$s.

### Application of Newton's second law to fluids: Euler's equation*
We are now almost ready to write Newton's Second Law for a small fluid element (a small volume of the flowing fluid). In the previous section we tackled the acceleration, and we have already found an expression for the mass in {numref}`sec:continuityeq`. The only factor we still need is the force&nbsp;$\bm{F}$ on the fluid element. In fluids, however, we usually do not measure forces but pressures - forces per unit area. The surrounding fluid is exerting this pressure (which can vary in space and time) on each of the bounding surfaces of our fluid element. To find the total force, similar to finding the total flux in {numref}`sec:continuityeq`, we simply need to add all the pressures times the relevant surface area - which in the general case again becomes a surface integral. In turn, that surface integral can also be rewritten as a volume integral, which gives us:

$$
\bm{F} = - \oint p \,\mathrm{d}\bm{A} = - \int (\bm{\nabla} p)\, \mathrm{d}V.
$$ (fluidforce)

Note the minus sign: if the pressure increases in some direction, the force on our fluid element is opposite that direction. By Newton's second law of motion (eq.&nbsp;{eq}`N2b`) we have

$$
\bm{F} = m \bm{a} = m \frac{\mathrm{D}\bm{v}}{\mathrm{D}t} = \int \rho \mathrm{d}V \frac{\mathrm{D}\bm{v}}{\mathrm{D}t}.
$$ (N2fluid)

Equating the expressions for the force given by&nbsp;{eq}`fluidforce` and {eq}`N2fluid` for an infinitesimal volume element $\mathrm{d}V$, we find:

$$
- \bm{\nabla} p = \rho \frac{\mathrm{D}\bm{v}}{\mathrm{D}t} = \rho \left[ \frac{\partial \bm{v}}{\partial t} + (\bm{v} \cdot \bm{\nabla}) \bm{v} \right].
$$ (Eulerfluid)

```{index} Euler's equation for ideal fluids
```
Equation&nbsp;{eq}`Eulerfluid` is Newton's second law applied to ideal fluids, and is known as **Euler's equation**.

When deriving Euler's equation, we took only internal forces in the fluid into account (equation&nbsp;{eq}`fluidforce`). However, it is also possible that an external force acts on the fluid as well, particularly gravity. These forces can simply be added to the right-hand side of&nbsp;{eq}`fluidforce`. Because we consider the forces on an infinitesimal fluid element $\mathrm{d}V$, they appear in equation&nbsp;{eq}`Eulerfluid` as forces per unit volume (which is the dimension of all terms in that equation). The force of gravity is then given by $\rho \bm{g}$, with $\bm{g}$ pointing downwards as usual, and Euler's equation becomes:

$$
- \nabla p + \rho \bm{g} = \rho \left[ \frac{\partial \bm{v}}{\partial t} + (\bm{v} \cdot \nabla) \bm{v} \right]
$$ (Eulerfluidwithgravity)

Note that from equation&nbsp;{eq}`Eulerfluidwithgravity` we can derive once more the expression for the hydrostatic pressure of {numref}`sec:hydropressure`. Because there the fluid is static, we simply have $\bm{v} = 0$, so equation&nbsp;{eq}`Eulerfluidwithgravity` becomes $\nabla p = \rho \bm{g}$, the solution of which is given by equation&nbsp;{eq}`hydropressure`: $p = p_0 + \rho g z$, where $z$ is the depth and $p_0$ the surface pressure.

(sec:NSeq)=
## Viscous fluids: Navier-Stokes equations

(sec:fluidstress)=
### The fluid stress tensor

```{index} ideal fluid, viscous flow
```
Bernoulli's and Euler's equations apply to ideal fluids, for which there is no friction and thus energy is conserved. However, as you'll immediately notice when you're trying to walk half-submerged in a swimming pool, ignoring friction in fluid flow is not a very good approximation. We therefore need to include frictional forces in our description of the flows, which means that the energy of the flow is no longer conserved - and we lose Bernoulli's equation. However, Newton's Second Law of motion still applies, we just need to add the frictional forces. Doing so will lead to the fundamental set of equations describing fluid flow, the Navier-Stokes equations. The Navier-Stokes equations describe *viscous* flows, which simply means that we take friction into account (*viscosity* being a measure of the frictional force).

For the most general version of Euler's equation, the forces originate from two sources: pressure gradients in the fluid and body forces acting on the fluid. In this section we'll consider the forces acting on a small fluid element $\mathrm{d}V$, and take all forces per unit volume (indicated by using small instead of capital letters). We write the force vector $\bm{f}$ as $(f_1, f_2, f_3)$ and indicate by $f_i$ its $i$th component; likewise, $b_i$ refers to the $i$th component of the body force $\bm{b}$, and $\partial_i p$ is the $i$th component of $\nabla p$, the gradient of the pressure. For Euler's equation we had:

$$
f_i = - \partial_i p + b_i.
$$ (Eulerforce)

We now wish to include frictional forces. For an ideal fluid, the only internal components of the force are the pressure gradients, which act perpendicular to the bounding surface of a fluid element. However, as we've seen in {numref}`sec:stress`, if there is friction, you can also have *shear flows*: forces acting along a bounding surface can cause a flow ({numref}`fig:plateshear`). Therefore, for each component of the force, we need to consider its action not only perpendicular to the appropriate surface, but also parallel to it. For each of the three directions the force can have, there is a surface perpendicular to it (e.g. the $yz$-plane for a force in the $x$-direction), so for each component of the force, we now get three contributions: one perpendicular to its plane and two parallel to it (see {numref}`fig:stresstensor`), so we end up with nine components. To illustrate, {numref}`fig:stresstensor` also shows what happens in 2D, when each direction only has one direction perpendicular to it, and we end up with four components.

The four (2D) or nine (3D) components we get when combining the forces with the surface directions together form the *stress tensor*. The stress tensor can be represented as a matrix, so its elements get two indices - row&nbsp;$i$ and column&nbsp;$j$. Note that a tensor is not identical to a matrix - if we change our coordinate system, the components of the stress tensor change along with it, just like the components of a vector depend on the coordinate system. In fact, vectors and tensors are closely related, and we sometimes call vectors 1-tensors, and what we call tensors here are 2-tensors (there are $n$-tensors for any integer number $n$).

In analogy to equation&nbsp;{eq}`Eulerforce` for the force in Euler's equation, where the force is the gradient of the pressure, we define the general fluid stress tensor such that, again, its gradient equals the force:

$$
\bm{f} = \nabla \cdot \bm{\sigma},
$$ (defstresstensorgradient)

or written in components

$$
f_i = \sum_j \partial_j \sigma_{ij}.
$$ (stresstensorgradientcomponents)

Remember that the $i$ and $j$ are simply indices that run from 1 to 3; we use them because they're easier to sum than $x$, $y$ and $z$. It may help to see equation&nbsp;{eq}`stresstensorgradientcomponents` as a matrix equation:

$$
\left(\begin{array}{c} f_1 \\ f_2 \\ f_3 \end{array}\right) = 
\left(\partial_1, \partial_2, \partial_3 \right) \cdot 
\left(\begin{array}{ccc} \sigma_{11} & \sigma_{12} & \sigma_{13} \\ \sigma_{21} & \sigma_{22} & \sigma_{23} \\  \sigma_{31} & \sigma_{32} & \sigma_{33} \end{array}\right).
$$ (stresstensorgradientmatrix)

To see how this definition connects back to Euler's equation, take $\sigma_{ij} = - p \delta_{ij}$, where $\delta_{ij}$ is the Kronecker delta, which is equal to 1 if $i=j$ and 0 otherwise; its matrix representation is simply the identity matrix, and with this identification equation&nbsp;{eq}`stresstensorgradientcomponents` becomes simply $f_i = -\partial_i p$. 

If you read {numref}`sec:stressstraintensors` you may wonder why the definition of the stress tensor given here seems different from the one given there. It's not - the one given there was for a force&nbsp;$\bm{F}$ acting on a surface with normal $\bm{\hat{n}}$, the one given here is for the force per unit volume acting on that volume. If you wish to calculate the actual force on a surface from the force per unit volume given in equation&nbsp;{eq}`defstresstensorgradient`, you have to integrate over the volume:

$$
\bm{F} = \int \bm{f} \mathrm{d}V = \int \nabla \cdot \bm{\sigma} \mathrm{d}V = \oint \bm{\sigma} \cdot \mathrm{d}\bm{A} = \oint \bm{\hat{n}} \cdot \bm{\sigma} \mathrm{d}A,
$$ (stressforcerelation)

where we've used Stokes' theorem in the third equality. From equation&nbsp;{eq}`stressforcerelation` we learn that we can interpret the stress tensor as follows: the force $\bm{F}$ on the surface with normal $\bm{\hat{n}}$ and area $\mathrm{d}A$ is given by $\bm{\hat{n}} \cdot \bm{\sigma} \mathrm{d}A$.

### Fluid equation of motion
We already found in {numref}`sec:idealfluids` that for fluid flows, Newton's second law of motion translates to the statement that the force per unit volume must be equal to the density times the material derivative of the velocity, see e.g. equations&nbsp;{eq}`Eulerfluid` and {eq}`Eulerfluidwithgravity`. We now substitute our general expression for the force as the gradient of the stress tensor plus any body forces (usually gravity) to get the general equation of motion for a fluid:

$$
\nabla \cdot \bm{\sigma} + \bm{b} = \rho \left[ \partial_t \bm{v} + \left(\bm{v} \cdot \nabla\right) \bm{v} \right],
$$ (fluideom)

or in components:

$$
\partial_j \sigma_{ij} + b_i = \rho \left[ \partial_t v_i + v_j \partial_j v_i \right].
$$ (fluideomcomp)

As we've seen above, substituting $\sigma_{ij} = - p \delta_{ij}$ and $b_i = \rho g_i$ gives us Euler's equation as a special case.

### Incompressible viscous fluids
For an ideal flow, $\sigma_{ij} = - p \delta_{ij}$ ({numref}`sec:idealfluids`), and for a viscous shear flow we've seen in {numref}`sec:shearflow` that the stress scales linearly with the strain rate: $\sigma = \eta \dot{\gamma}$ (equation&nbsp;{eq}`fluidstressstrainrate`). Although the relation in {numref}`sec:shearflow` applied only to the specific case of a plate shear experiment, it can be generalized to stresses and flows in all directions. For a viscous, incompressible flow, we then have for the stress tensor:

```{math}
:label: viscousstresstensor
\begin{align*}
\sigma_{ij} &= -p \delta_{ij} + \sigma_{ij}', \\
\sigma_{ij}' &= \eta \dot{\gamma}_{ij}, \\
\dot{\gamma}_{ij} &= \partial_i v_j + \partial_j v_i.
\end{align*}
```

Substituting&nbsp;{eq}`viscousstresstensor` into the equations of motion&nbsp;{eq}`fluideom`, we find the **Navier-Stokes equations** for incompressible fluids:

$$
\rho \left[ \partial_t \bm{v} + \left(\bm{v} \cdot \nabla\right) \bm{v} \right] = - \nabla p + \eta \Delta v + \bm{f}^\mathrm{ext}.
$$ (NS)

On the right-hand side, we end up with the Laplacian of the velocity because the force is the derivative of the stress, which is linear in the strain rate, which is itself a derivative of the velocity. You may note that we dropped a term $\nabla(\nabla \cdot \bm{v})$ on the right-hand side of equation&nbsp;{eq}`NS`; we can do this because the fluid is incompressible, and we've tacitly assumed that its density doesn't change over time, so $\nabla \cdot \bm{v} = 0$ ({numref}`sec:continuityeq`). You can of course derive an even more general equation for fluids which are compressible, in which case you have not one but two types of viscosity, and which can be found in advanced fluid mechanics textbooks<sup>[^3]</sup>.

(sec:Stokeseq)=
## Flow at low Reynolds number: Stokes' equation

The Navier-Stokes equations apply to all incompressible fluid flows, and thus are very generic. Unfortunately, they are also very hard to solve - particularly because they contain a term which is nonlinear: the second term on the left of equation&nbsp;{eq}`NS`. Fortunately, there is a limit case in which we can ignore this term: that of low Reynolds number.

The Reynolds number is a nondimensional number, which compares the relative importance of the two kinds of forces present in a fluid flow: conservative inertial forces and frictional viscous forces. For a flow with a typical speed $v$ inside a container with size&nbsp;$L$ (or around a body with size&nbsp;$L$), the Reynolds number $\Rn$ is given by:

$$
\Rn = \frac{\text{inertial terms}}{\text{viscous terms}}= \frac{\rho v L}{\eta}.
$$ (defReynoldsnumber)

A few typical values of the Reynolds number for swimmers and other moving objects of various sizes are given in {numref}`table:Reynoldsnumbers`.

```{table} Some typical Reynolds numbers.
:name: table:Reynoldsnumbers
| Object | $\Rn$ |
| :--- | :--: |
| Oil tanker | $10^8$ |
| Swimming human | $10^4$ |
| Swimming fish | $10^2$ |
| Swimming bacterium | $10^{-4}$ |
```

To see how the Reynolds number emerges from the Navier-Stokes equations, we need to make order-of-magnitude estimates of the various terms. If our flow is characterized by a lengthscale&nbsp;$L$ and velocity&nbsp;$V$, we also have a natural timescale, $L/V$. The inertial term is the left-hand side of equation&nbsp;{eq}`NS` (remember, this comes from '$ma$' on the right-hand side of Newton's second law), which now consists of two terms, the magnitude of which we can estimate as:

```{math}
\begin{align*}
\left| \rho \partial_t \bm{v} \right| &\approx \rho v^2/L, \\
\left| \rho (\bm{v} \cdot \nabla) \bm{v} \right| &\approx \rho v^2/L,
\end{align*}
```

so they are roughly equal. Note that we included the derivatives in our estimate, since taking the derivative with respect to a quantity&nbsp;$x$ adds a dimension&nbsp;$1/x$ to whatever you took the derivative of. We find the viscous term on the right-hand side of equation&nbsp;{eq}`NS`, and we can estimate its magnitude as:

$$
\left| \eta \Delta \bm{v} \right| \approx \eta V / L^2.
$$

Now taking the ratio of the magnitudes of the inertial to the viscous terms we get the Reynolds number&nbsp;{eq}`defReynoldsnumber`.

Our order-of-magnitude estimate has immediate consequences, because it tells us that we can simplify the Navier-Stokes equations in two important cases: those of a very large or a very small Reynolds number. For a very large Reynolds number, the viscous term hardly contributes, so we can drop it, and retrieve the Euler equation for ideal (frictionless) fluid flow. For a very small Reynolds number, the inertial terms don't matter, so we drop those, and find a new set of equations, known as **Stokes' equations**:

$$
-\nabla p + \eta \Delta \bm{v} + \bm{f}^\mathrm{ext} = 0.
$$ (Stokeseq)

Stokes' equations are a lot easier to deal with than the complete Navier-Stokes equations. For starters, they are *linear*, which not only makes finding a solution much easier, it also allows you to combine solutions: the sum of two solutions is again a solution (this is known as the principle of *superposition*). Second, Stokes' equations are *time-independent*, which means the solution&nbsp;$\bm{v}(\bm{x})$ is a function of the position alone, unless the external force depends on time (but then solutions can be again found by superposition for each time interval in which the external force is constant). You may have noticed that Stokes' equations contain four unknowns (the three components of $\bm{v}$, and $p$), but are only three equations (one for each component of $\bm{v}$), so we need an additional equation to close the system - which is provided by the continuity equation&nbsp;{eq}`continuitygeneral`, or, more commonly, by its incompressible version&nbsp;{eq}`continuity`:

$$
\nabla \cdot \bm{v} = 0.
$$ (Stokesincompressibility)

### Stokes' drag
We already encountered Stokes' Law for the drag force on a spherical object moving through a fluid ({numref}`sec:frictiondrag`, equation&nbsp;{eq}`FStokes`):

$$
\bm{F}_d = -6 \pi \eta R \bm{v},
$$ (FStokes2)

where $R$ is the object's radius and $\bm{v}$ its velocity. Equation&nbsp;{eq}`FStokes2` follows from Stokes' equations, by calculating the flow field past a spherical object, and then making a frame of reference transformation from the flow to the object - the same as going from the lab frame to the center-of-mass frame in collision experiments ({numref}`sec:collisions`). We won't perform the calculation here as it is quite complicated mathematically, but it can be found in any advanced fluid mechanics textbook.

### Life at low Reynolds number
As is immediately obvious from {numref}`table:Reynoldsnumbers`, if we're studying swimming humans or fish we cannot ignore inertia (and thus are stuck with the full nonlinear Navier-Stokes equations), but if we study the swimming behavior of a cell or bacterium we're in business with Stokes' equations. This simple observation has important consequences: life for these small organisms is very different from life for us, because the laws of physics appear to be different! When we throw something or kick off from the side of a swimming pool, we keep moving for a while, and especially in air we often even neglect drag when calculating the ensuing motion. Also, we can swim using the breast stroke, gaining momentum by pushing back the water with our arms, and losing less when we push them forward again, all because we have inertia. But as we've just seen, for the micron-sized bacteria and cells inertia doesn't matter at all! Their world is dominated by drag forces, and if they'd have developed physics, their basic law would not be Newton's second law of motion, because that law represents inertia. Instead, their basic law would be Stokes' law, so the force scales with the velocity, not the acceleration.

Although bacteria (almost certainly) did not put any effort in deriving the laws of physics that apply to their world, they are still subject to them. Therefore, they cannot swim using the same methods we do. Take the breaststroke: because they have no inertia, even though they might gain some momentum by pushing any extremities backward, they'd lose it again right away when bringing them back forward. Worse, they'd end up in exactly the same position they started. Bacteria therefore cannot swim employing a back-and-forth motion, but have to use different techniques. One that they commonly employ is to build a corkscrew-shaped tail, which they rotate, instead of moving something back-and-forth. Because the corkscrew is *helical*, it is *chiral* (that is, there are a left-hand version and a right-hand version, which cannot be transferred into each other by rotation, only by mirroring), and rotating it in one direction will allow the bacterium to generate forces in the same direction continuously.

## Problems
```{exercise} A stone covered in ice
:class: dropdown
Suppose we have a stone covered in ice that floats once we put it in water. We place the ice-covered stone in a measuring glass. The water level rises from $10.00\;\mathrm{L}$ to $13.85\;\mathrm{L}$. Once all the ice has molten, the water level reads $13.70\;\mathrm{L}$. Finally, we remove the stone from the glass and the water level drops further to  $13.60\;\mathrm{L}$. You may use that $\rho_\mathrm{water} = 1000\;\mathrm{kg}/\mathrm{m}^3$ $\rho_\mathrm{ice} = 900\;\mathrm{kg}/\mathrm{m}^3$.
1. What is the mass of the stone?
1. Which percentage of the ice should melt before the ice and stone are completely submerged?
1. What is the water level read that happens?
```

```{exercise} Physics in living trees
:class: dropdown
1. Suppose you want to drink your glass of water through a straw. Show that the theoretical limit to the
height you can be above your glass and still be able to drink it is $h = 10.2\;\mathrm{m}$.
    Trees seem to violate the the laws of physics by being taller than $10.2\;\mathrm{m}$. The reason trees can transport water this high up is that at the top of the tree, the water undergoes a phase transition from water to
    vapor. Consequently, in its leaves, a tree can create an effective negative pressure.
1. Ignoring both frictional and capillary forces, find the (negative) pressure needed to transport water to a height of $100\;\mathrm{m}$.
    The tree is growing pine cones. The pine cones are attached to the tree via a twig with a length of $5.0\;\mathrm{cm}$, and a cross-sectional area of $1.0\;\mathrm{mm}^2$. On the ground we only find pine cones with a mass of $0.50\;\mathrm{kg}$.
1. What is the ultimate strength of the twig connecting the pine cone to the tree?
```

````{exercise} Streamlines
:class: dropdown
The two-dimensional steady flow of a fluid with density $\rho$ is given by
```{math}
\bm{v}(x,y) = K \frac{-y \bm{\hat{x}} + x \bm{\hat{y}}}{x^2 + y^2},
```
where $K$ is a constant.
1. Can this flow correspond to the flow of an incompressible fluid?
1. Determine and sketch the streamlines of this flow.
1. Determine and sketch (in the same figure as (b)) the acceleration field of this flow.
1. Determine the pressure difference between two points, a distance $r_1$ and $r_2 > r_1$ away from the origin. (Hint: Start by taking two points on the positive $x$-axis and then generalize using a symmetry argument.) What happens at the origin?
````

````{exercise} Water flowing from an oscillating source
:class: dropdown
Water flowing out of an oscillating sprinkler head produces a velocity field given by
```{math}
\bm{v} = v_{x0} \sin\left[\omega\left(t - \frac{y}{v_{y0}}\right)\right] \bm{\hat{x}} + v_{y0} \bm{\hat{y}},
```
where $v_{x0}$, $v_{y0}$, and $\omega$ are constants. The $y$-component of the velocity is thus a constant, while the $x$-component of the velocity at $y = 0$ coincides with the velocity of the oscillating sprinkler head.
1. Determine and sketch the streamlines (the line tangent to the velocity field) that passes through the origin at $t = 0$, and the one that passes through the origin at $t = \pi/2\omega$.
1. Determine and sketch the pathline (the path followed by a single fluid particle) of a particle that was at the origin at $t = 0$ and for a particle that was at the origin at $t = \pi/2\omega$.
````

[^1]: They are not totally incompressible - if they were, there would be no sound under water. But apart from sound waves, for all relevant phenomena the incompressibility approximation is a very good one.

[^2]: To get&nbsp;{eq}`streamlines` from&nbsp;{eq}`streamlinesparametrized`, we apply the chain rule:
	```{math}
	\frac{\mathrm{d}x}{\mathrm{d}y} = \frac{\mathrm{d}x}{\mathrm{d}s} \frac{\mathrm{d}s}{\mathrm{d}y} = v_x \cdot \frac{1}{v_y} = \frac{v_x}{v_y},
	```
	which gives the first identity of&nbsp;{eq}`streamlines`.

[^3]: See, for example, Batchelor, or Landau and Lifschitz.

