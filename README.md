# Introduction to particle and continuum mechanics

Source files for the open textbook *Introduction to particle and continuum mechanics*. This book is a [Jupyter book](https://jupyterbook.org/intro.html). To build it, install the packages in requirements.txt, then run `jb build ./`

## Authors and acknowledgment
This book was written by Dr. [Timon Idema](https://idemalab.tudelft.nl), associate professor at TU Delft

## License
This book is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
